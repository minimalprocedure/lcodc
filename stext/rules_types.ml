module EmittedTypes = struct
  type t =
    | Literal of string
    | Strong of t list
    | Emphasis of string
    | Underline of string
    | Heading1 of string
    | Heading2 of string
    | HyperLink of t list
    | HyperLinkImage of t list
    | Paragraph of t list
    | HyperLinkTitle of string
    | HyperLinkUrl of string
    | SoftBreak of string
    | List of t list
    | ListElement of string
end

module RuleTypes = struct
  type t =
    | Literal
    | Strong
    | Emphasis
    | Underline
    | Heading1
    | Heading2
    | HyperLink
    | HyperLinkImage
    | Paragraph
    | HyperLinkTitle
    | HyperLinkUrl
    | SoftBreak
    | List
    | ListElement
end

module PhaseTypes = struct
  type t =
    | Literal of char list
    | Delimited of char list
end

type rule_state =
  | FailRule of char list * char list
  | SuccessRule of char list * char list

type delimiters =
  | Delim of char list
  | EmptyDelim

type rule =
  { start : delimiters
  ; stop : delimiters
  ; t : RuleTypes.t
  ; emit : char list -> EmittedTypes.t
  }

let delim_or_empty delim =
  match delim with
  | Delim delim -> delim
  | EmptyDelim -> []
;;
