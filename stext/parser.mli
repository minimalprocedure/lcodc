val scanner
  :  char list
  -> Rules_types.rule list * Rules_types.rule
  -> Rules_types.EmittedTypes.t list

val parse
  :  string
  -> Rules_types.rule list * Rules_types.rule
  -> Rules_types.EmittedTypes.t list
