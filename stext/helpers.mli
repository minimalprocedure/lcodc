val read_file : string -> string
val write_text_to_file : string -> string -> unit
val implode : char list -> string
val explode : string -> char list
