open Helpers
open Rules_types

let starts_with prefix chars =
  let rec scan_delim = function
    | [], chars -> Some chars
    | p :: prefix, c :: chars when p = c -> scan_delim (prefix, chars)
    | _ -> None
  in
  match prefix with
  | EmptyDelim -> None
  | Delim prefix -> scan_delim (prefix, chars)
;;

let delimited rule chars =
  let { start; stop; _ } = rule in
  let rec get_body acc chars =
    match starts_with stop chars, chars with
    | Some chars, _ -> Some (SuccessRule (List.rev acc, chars))
    | _, c :: rest -> get_body (c :: acc) rest
    | _, _ ->
      let prefix = delim_or_empty start in
      Some (FailRule (prefix, List.rev acc))
  in
  match starts_with start chars with
  | Some chars -> get_body [] chars
  | _ -> None
;;

let status (rules, defrule) chars =
  let is opt =
    match opt with
    | None -> false
    | Some _ -> true
  in
  let rule =
    List.find_opt
      (fun rule -> rule.start <> EmptyDelim && is (starts_with rule.start chars))
      rules
  in
  match rule with
  | None -> PhaseTypes.Literal chars, defrule
  | Some rule -> PhaseTypes.Delimited chars, rule
;;

let scanner chars (rules, defrule) =
  let rec parser span acc chars =
    let apply_rule rule span =
      let emit rule delim_value =
        match delim_value with
        | delim, (_ :: _ as chars) ->
          let span = rule.emit delim :: span in
          parser span [] chars
        | delim, [] -> rule.emit delim :: span
      in
      match delimited rule chars with
      | None -> parser span acc chars
      | Some (SuccessRule (delim, value)) -> emit rule (delim, value)
      | Some (FailRule (delim, value)) -> emit defrule (delim, value)
    in
    let apply_defrule ?c () =
      match c with
      | Some c -> defrule.emit (List.rev (c :: acc)) :: span
      | _ -> if acc = [] then span else defrule.emit (List.rev acc) :: span
    in
    match status (rules, defrule) chars with
    | PhaseTypes.Literal (c :: rest), _rule ->
      (match rest with
      | [] -> apply_defrule ~c ()
      | _ -> parser span (c :: acc) rest)
    | _, rule -> apply_defrule () |> apply_rule rule
  in
  parser [] [] chars |> List.rev
;;

let parse text rules = scanner (explode text) rules
