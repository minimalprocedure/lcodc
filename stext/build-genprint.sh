#!/bin/bash
rm .genprint.cache
cp dune.genprint dune
dune clean
dune build stext.exe
cp ./_build/default/.stext.eobjs/byte/dune__exe__Helpers.cmt ./_build/default/Helpers.cm
cp ./_build/default/.stext.eobjs/byte/dune__exe__Html.cmt ./_build/default/Html.cmt
cp ./_build/default/.stext.eobjs/byte/dune__exe__Parser.cmt ./_build/default/Parser.cmt
cp ./_build/default/.stext.eobjs/byte/dune__exe__Rules.cmt ./_build/default/Rules.cmt
cp ./_build/default/.stext.eobjs/byte/dune__exe__Rules_types.cmt ./_build/default/Rules_types.cmt
cp ./_build/default/.stext.eobjs/byte/dune__exe__Stext.cmt ./_build/default/Stext.cmt
cp dune.release dune
