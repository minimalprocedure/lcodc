open Printf
open Rules_types.EmittedTypes

let literal s = sprintf "%s" s
let heading1 s = sprintf "<h1>%s</h1>" s
let heading2 s = sprintf "<h2>%s</h2>" s

(*let heading3 s = sprintf "<h3>%s</h3>" s
let heading4 s = sprintf "<h4>%s</h4>" s
let heading5 s = sprintf "<h5>%s</h5>" s
let heading5 s = sprintf "<h6>%s</h6>" s
*)
let emphasis s = sprintf "<em>%s</em>" s
let underline s = sprintf "<u>%s</u>" s
let softbreak s = if s = "" then "<br />" else sprintf "<br />%s<br />" s
let list_element s = sprintf "\n<li>%s</li>" s

let hyperLink tree =
  match tree with
  | HyperLinkTitle t :: HyperLinkUrl u :: _ ->
    sprintf {|<a href="%s" title="%s">%s</a>|} u t t
  | _ -> ""
;;

let hyperLink_image tree =
  match tree with
  | HyperLinkTitle t :: HyperLinkUrl u :: _ ->
    sprintf {|<img src="%s" title="%s" alt="%s" />|} u t t
  | _ -> "aaa"
;;

let rec strong tree =
  let match_emits acc e =
    let acc, _ = match_span_emits (acc, e) in
    acc
  in
  let elements = List.fold_left match_emits [] tree |> List.rev in
  sprintf "<strong>%s</strong>" (String.concat "" elements)

and paragraph tree =
  let match_emits acc e =
    let acc, _ = match_span_emits (acc, e) |> match_block_emits in
    acc
  in
  let elements = List.fold_left match_emits [] tree |> List.rev in
  sprintf "\n<p>\n%s\n</p>\n" (String.concat "" elements)

and list tree =
  let match_emits acc e =
    let acc, _ = match_span_emits (acc, e) |> match_block_emits in
    acc
  in
  let elements = List.fold_left match_emits [] tree |> List.rev in
  sprintf "\n<ul>\n%s\n</ul>\n" (String.concat "" elements)

and html tree =
  let match_emits acc e =
    let acc, _ = match_span_emits (acc, e) |> match_block_emits in
    acc
  in
  let elements = List.fold_left match_emits [] tree |> List.rev in
  sprintf "<html>\n<head>\n<title>titolo</title>\n</head>\n<body>%s</body>\n</html>" (String.concat "" elements)

and match_block_emits (acc, e) =
  match e with
  | Strong elements -> strong elements :: acc, e
  | Paragraph tree -> paragraph tree :: acc, e
  | List tree -> list tree :: acc, e
  | _ -> acc, e

and match_span_emits (acc, e) =
  match e with
  | Literal s -> literal s :: acc, e
  | Heading1 s -> heading1 s :: acc, e
  | Heading2 s -> heading2 s :: acc, e
  | Emphasis s -> emphasis s :: acc, e
  | Underline s -> underline s :: acc, e
  | HyperLink link -> hyperLink link :: acc, e
  | HyperLinkImage link -> hyperLink_image link :: acc, e
  | SoftBreak s -> softbreak s :: acc, e
  | ListElement s -> list_element s :: acc, e
  | _ -> acc, e
;;
