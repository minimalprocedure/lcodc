open Helpers
open Rules_types
open Parser

let def_rule () =
  { t = RuleTypes.Literal
  ; start = EmptyDelim
  ; stop = EmptyDelim
  ; emit = (fun chars -> EmittedTypes.Literal (implode chars))
  }
;;

let stext_hyperlink_subrules () =
  [ { t = RuleTypes.HyperLinkTitle
    ; start = Delim [ '[' ]
    ; stop = Delim [ ']' ]
    ; emit = (fun chars -> EmittedTypes.HyperLinkTitle (implode chars))
    }
  ; { t = RuleTypes.HyperLinkUrl
    ; start = Delim [ '(' ]
    ; stop = Delim [ ')' ]
    ; emit = (fun chars -> EmittedTypes.HyperLinkUrl (implode chars))
    }
  ]
;;

let stext_list_subrules () =
  [ { t = RuleTypes.ListElement
    ; start = Delim [ '+'; ' ' ]
    ; stop = Delim [ '\n' ]
    ; emit = (fun chars -> EmittedTypes.ListElement (implode chars))
    }
  ]
;;

let rec stext_rules () =
  [ { t = RuleTypes.Paragraph
    ; start = Delim [ '$'; ' ' ]
    ; stop = Delim [ '\n'; '\n' ]
    ; emit =
        (fun chars ->
          let rules =
            List.filter (fun r -> r.t <> RuleTypes.Paragraph) (stext_rules ())
          in
          let para = scanner chars (rules, def_rule ()) in
          EmittedTypes.Paragraph para)
    }
  ; { t = RuleTypes.List
    ; start = Delim [ '('; '+'; ' ' ]
    ; stop = Delim [ ')' ]
    ; emit =
        (fun chars ->
          let chars = [ '+'; ' ' ] @ chars @ [ '\n' ] in
          let para = scanner chars (stext_list_subrules (), def_rule ()) in
          EmittedTypes.List para)
    }
  ; { t = RuleTypes.Heading1
    ; start = Delim [ '#'; ' ' ]
    ; stop = Delim [ '\n'; '\n' ]
    ; emit = (fun chars -> EmittedTypes.Heading1 (implode chars))
    }
  ; { t = RuleTypes.Heading2
    ; start = Delim [ '#'; '#'; ' ' ]
    ; stop = Delim [ '\n'; '\n' ]
    ; emit = (fun chars -> EmittedTypes.Heading2 (implode chars))
    }
  ; { t = RuleTypes.Strong
    ; start = Delim [ '*' ]
    ; stop = Delim [ '*' ]
    ; emit =
        (fun chars ->
          let rules =
            List.filter (fun r -> r.t <> RuleTypes.Strong) (stext_rules ())
          in
          let parts = scanner chars (rules, def_rule ()) in
          EmittedTypes.Strong parts)
    }
  ; { t = RuleTypes.Emphasis
    ; start = Delim [ '/' ]
    ; stop = Delim [ '/' ]
    ; emit = (fun chars -> EmittedTypes.Emphasis (implode chars))
    }
  ; { t = RuleTypes.Underline
    ; start = Delim [ '_' ]
    ; stop = Delim [ '_' ]
    ; emit = (fun chars -> EmittedTypes.Underline (implode chars))
    }
  ; { t = RuleTypes.HyperLink
    ; start = Delim [ '[' ]
    ; stop = Delim [ ')' ]
    ; emit =
        (fun chars ->
          let chars = [ '[' ] @ chars @ [ ')' ] in
          let hyper = scanner chars (stext_hyperlink_subrules (), def_rule ()) in
          EmittedTypes.HyperLink hyper)
    }
  ; { t = RuleTypes.HyperLinkImage
    ; start = Delim [ '!'; '[' ]
    ; stop = Delim [ ')' ]
    ; emit =
        (fun chars ->
          let chars = [ '[' ] @ chars @ [ ')' ] in
          let hyper = scanner chars (stext_hyperlink_subrules (), def_rule ()) in
          EmittedTypes.HyperLinkImage hyper)
    }
  ; { t = RuleTypes.SoftBreak
    ; start = Delim [ '\\' ]
    ; stop = Delim [ '\\' ]
    ; emit = (fun chars -> EmittedTypes.SoftBreak (implode chars))
    }
  ]
;;
