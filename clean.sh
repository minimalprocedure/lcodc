#!/bin/bash

for d in $(ls -d */); do 
  if [ -f "$d/clean.sh" ]; then
    proj=${d%%/}
    cd $d
    echo "Clean (clean.sh) $proj"
    ./clean.sh
    cd ..
  elif [ -f "$d/dune" ]; then
    proj=${d%%/}
    cd $d
    echo "Clean (dune) $proj"
    dune clean
    cd ..
  fi
done

