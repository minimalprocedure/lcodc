(** * Copyright (c) 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

let load_file fpath =
  let input = In_channel.open_text fpath in
  let text = In_channel.input_all input in
  In_channel.close input;
  Bytes.of_string text
;;

let bits n =
  let bit n i =
    match n land (1 lsl (i - 1)) with
    | 0 -> 0
    | _ -> 1
  in
  List.map (bit n) (List.init 8 (fun e -> e + 1) |> List.rev)
;;

let bitize bytes =
  Bytes.fold_left
    (fun acc b ->
      let c = Char.code b in
      bits c :: acc)
    []
    bytes
  |> List.rev
  |> List.flatten
;;

let execute ~delay ~op0 ~op1 bits =
  List.iter
    (fun b ->
      match b with
      | 0 ->
        ignore (op0 ());
        ignore (Unix.sleepf delay)
      | _ ->
        ignore (op1 ());
        ignore (Unix.sleepf delay))
    bits
;;
