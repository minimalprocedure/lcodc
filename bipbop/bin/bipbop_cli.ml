(** * Copyright (c) 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

let infile = ref ""
let aop0 = ref "Bip"
let aop1 = ref "Bop"
let delay = ref 0.0
let usage = "bepbop-cli usage:\n\n" ^ Sys.argv.(0) ^ " --in filename [options] \n"

let _args_parse =
  let opts =
    [ "--in", Arg.Set_string infile, ": bepbop file"
    ; "--aop0", Arg.Set_string aop0, ": text op0"
    ; "--aop1", Arg.Set_string aop1, ": text op1"
    ; "--delay", Arg.Set_float delay, ": delay"
    ]
  in
  let anons _ = () in
  Arg.parse opts anons usage
;;

let () =
  let source = Bipbop.load_file !infile in
  let op0 () = Printf.printf "%s\n%!" (Ops.op0 !aop0) in
  let op1 () = Printf.printf "%s\n%!" (Ops.op1 !aop1) in
  Bipbop.execute ~delay:!delay ~op0 ~op1 (source |> Bipbop.bitize)
;;
