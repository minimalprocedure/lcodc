type t =
  { year : int
  ; month : int
  ; day : int
  ; hour : int
  ; minute : int
  ; second : int
  }

val hour : float -> int
val minute : float -> int
val second : float -> int
val day : float -> int
val month : float -> int
val year : float -> int
val now : unit -> t
val string_of_dt : t -> string