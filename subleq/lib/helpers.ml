(**
 * Copyright (c) 2023 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

let stream_of_ch ic =
  Seq.of_dispenser (fun _ ->
      try Some (input_line ic) with End_of_file -> None)

let string_of_file path =
  let ic = open_in path in
  let read acc line = Printf.sprintf "%s %s" acc line in
  Seq.fold_left read "" (stream_of_ch ic)
