open Subleqvm.Sq
open Subleqvm.Helpers

let source_path = ref ""

let _args_parse =
  let opts = [ ("-s", Arg.Set_string source_path, ": source file") ] in
  let usage = "subleq\nusage: " ^ Sys.argv.(0) ^ " [-s source file]" in
  let anons _ = () in
  Arg.parse opts anons usage

let () =
  let source =
    if !source_path = "" then hw_source
    else
      try string_of_file !source_path
      with e ->
        let msg = Printexc.to_string e in
        Printf.eprintf "Error opening source: %s\n" msg;
        hw_source
  in
  Source source |> run
