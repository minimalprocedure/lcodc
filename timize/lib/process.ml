(**
 * Copyright (c) 2023 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

type exetime =
  | TimeString of string
  | TimeFloat of float

type process_info =
  { pid : int
  ; program : string
  ; args : string
  ; output : string
  ; exit_status : string
  ; exit_code : int
  ; exetime : exetime
  }

let create_and_wait program args =
  let readp, writep = Unix.pipe ~cloexec:true () in
  let cpid =
    Unix.create_process
      program
      [| program; args |]
      Unix.stdin
      writep (*Unix.stdout*)
      Unix.stderr
  in
  Unix.close writep;
  let pid, status = Unix.waitpid [] cpid in
  let in_channel = Unix.in_channel_of_descr readp in
  let output = ref [] in
  (try
     while true do
       output := input_line in_channel :: !output
     done
   with
   | End_of_file -> ());
  Unix.close readp;
  let exit_status, exit_code =
    match status with
    | WEXITED code -> "exited", code
    | WSIGNALED code -> "signaled", code
    | WSTOPPED code -> "stopped", code
  in
  { pid
  ; program
  ; args
  ; output = !output |> List.rev |> String.concat "\n"
  ; exit_status
  ; exit_code
  ; exetime = TimeString ""
  }
;;

let time_process program args =
  let time, pinfo = Timers.time_n (fun () -> create_and_wait program args) in
  let exetime = TimeString (Printf.sprintf (Timers.time_print_format ()) time) in
  { pinfo with exetime }
;;
