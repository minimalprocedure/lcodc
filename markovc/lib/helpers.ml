(**
 * Copyright (c) 2023 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

let regexp string = Re.Perl.re ~opts:[ `Dotall ] string |> Re.Perl.compile

let replace_all regex by string = 
  Re.replace_string ~all:true regex ~by string 

let strip_first_last string =
  replace_all (regexp {|^.|.$|}) "" string

let strip_first string = 
  replace_all (regexp {|^.|}) "" string

let get_groups2 rmatch =
  let g = Re.Group.get rmatch in
  String.trim (g 1), String.trim (g 2)
;;

let escape_symbols text =
  let re = regexp {|\W|} in
  let f g =
    let symbols = Re.Group.all g |> Array.to_list in
    List.map (fun s -> Printf.sprintf {|\%s|} s) symbols |> String.concat ""
  in
  Re.replace ~all:true re ~f text
;;

let open_file path =
  let ic = In_channel.open_text path in
  let source = In_channel.input_all ic in
  In_channel.close ic;
  source
;;

let write_file file text =
  let out = Out_channel.open_text file in
  Out_channel.output_string out text;
  Out_channel.close out
;;
