open Sexplib
open Sexplib.Std

type scm_data =
  { axiom : string
  ; constants : string list
  ; variables : string list
  ; operators : string list
  ; rules : (string * string) list
  ; cycles : int option [@sexp.option]
  ; actions : (string * (string * string) list) list option [@sexp.option]
  }
[@@deriving sexp]

type expression =
  | Op of string
  | Variable of string
  | Constant of string
  | Value of expression list

type rule = Rule of (expression * expression)

module List = struct
  include List

  let first_opt = function
    | h :: _ -> Some h
    | [] -> None
  ;;

  let rec lookup k = function
    | [] -> None
    | (k', v) :: t -> if k = k' then Some v else lookup k t
  ;;

  let contains e l =
    match find_opt (fun e' -> e' = e) l with
    | Some _ -> true
    | _ -> false
  ;;
end

let string_of_char c = String.make 1 c
let list_of_string s = List.init (String.length s) (fun i -> s.[i] |> string_of_char)

let rule_lookup rule_key rules =
  let check = function
    | Rule (rk, _) ->
      (match rk with
      | Variable _ -> rk = rule_key
      | _ -> false)
  in
  List.find_opt check rules
;;

let rule_body = function
  | Rule (_, b) ->
    (match b with
    | Value v -> v
    | _ -> [])
;;

let exprs_flatten exprs =
  let rec explode acc = function
    | [] -> acc
    | h :: t ->
      let acc' =
        match h with
        | (Op _ | Variable _ | Constant _) as v -> v :: acc
        | Value v -> explode acc v
      in
      explode acc' t
  in
  explode [] exprs |> List.rev
;;

let exprs_values exprs =
  let exprs' = exprs_flatten exprs in
  let unbox acc v =
    match v with
    | Variable v' | Constant v' | Op v' -> v' :: acc
    | _ -> acc
  in
  (* Non importante fare un reverse, solo per correttezza *)
  List.fold_left unbox [] exprs' |> List.rev
;;

let parse str scm =
  let l = list_of_string str in
  List.(
    fold_left
      (fun acc e ->
        match e with
        | v when contains v scm.variables -> Variable v :: acc
        | c when contains c scm.constants -> Constant c :: acc
        | o when contains o scm.operators -> Op o :: acc
        | _ -> acc)
      []
      l
    |> rev)
;;

let parse_axiom scm = parse scm.axiom scm

let parse_cycles scm =
  match scm.cycles with
  | Some cycles -> cycles
  | _ -> 0
;;

let parse_rules scm =
  let fn acc r =
    let k, v = r in
    let k' =
      match List.first_opt (parse k scm) with
      | None -> Constant ""
      | Some key -> key
    in
    Rule (k', Value (parse v scm)) :: acc
  in
  (* Non importante fare un reverse, solo per correttezza *)
  List.(fold_left fn [] scm.rules |> rev)
;;

let parse_actions scm =
  let open CamelGraphics in
  let decode actions_dic a =
    let value, actions = a in
    let acts =
      List.(
        fold_left
          (fun actions action ->
            let fn, arg = action in
            (match fn with
            | "forward" -> forward (int_of_string arg)
            | "right_by" -> right_by (float_of_string arg)
            | "left_by" -> left_by (float_of_string arg)
            | "label" -> label arg
            | "text" -> text arg
            | "line_width" -> line_width (int_of_string arg)
            | "color" -> color2 (Color.HexColor arg)
            | "storepush" -> storepush
            | "storepop" -> storepop
            | _ -> noop ())
            :: actions)
          []
          actions)
    in
    (value, acts) :: actions_dic
  in
  match scm.actions with
  | None -> []
  | Some actions -> List.(fold_left decode [] actions)
;;

let rec rewrite sys rules cicles =
  let fn acc term =
    match term with
    | Variable _ ->
      (match rule_lookup term rules with
      | Some r -> List.rev_append (rule_body r) acc
      | None -> term :: acc)
    | Constant _ -> term :: acc
    | Value _ -> acc
    | Op _ -> term :: acc
  in
  let sys' = List.(fold_left fn [] sys |> rev) in
  if cicles <= 1 then sys' else rewrite sys' rules (cicles - 1)
;;

let lsystem_from_file filename = Sexp.load_sexp filename |> scm_data_of_sexp
let to_string exprs = String.concat "" (exprs_values exprs)

let rec to_string_ast exprs =
  let open Printf in
  (*let exprs' = exprs_flatten exprs in*)
  let unbox acc v =
    let kword =
      match v with
      | Variable v' -> sprintf "Variable(%s)" v'
      | Constant v' -> sprintf "Constant(%s)" v'
      | Op v' -> sprintf "Op(%s)" v'
      | Value v' -> "Value(" ^ to_string_ast v' ^ ")"
    in
    kword :: acc
  in
  (* Non importante fare un reverse, solo per correttezza *)
  List.(fold_left unbox [] exprs |> rev) |> String.concat ";"
;;

let to_graphics sx sy exprs actions =
  let exprs' = exprs_flatten exprs in
  let open CamelGraphics in
  let draw acc expr =
    (match expr with
    | Op v | Variable v | Constant v ->
      (match List.lookup v actions with
      | None -> [ noop () ]
      | Some a -> a)
    | Value _ -> [ noop () ])
    :: acc
  in
  let stps = List.(fold_left draw [] exprs' |> flatten |> rev) in
  try
    canvas sx sy;
    home () |> steps stps |> stop;
    ignore (read_line ())
  with
  | _ -> canvas_close ()
;;
