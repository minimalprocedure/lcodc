open Lsystem

let scm = ref ""
let print = ref false
let print_ast = ref false

let args_parse =
  let opts =
    [ "-r", Arg.Set_string scm, ": lsystem file"
    ; "-p", Arg.Set print, ": print"
    ; "-a", Arg.Set print_ast, ": print ast"
    ]
  in
  let usage =
    "L-System\nusage: "
    ^ Sys.argv.(0)
    ^ " [-r lsystem_rule_file]"
    ^ " [-p (print to console)]"
    ^ " [-a (print ast)]"
  in
  let anons x = scm := x in
  Arg.parse opts anons usage
;;

let () =
  match !scm with
  | "" -> print_endline "Missing filename"
  | _ ->
    let scm = lsystem_from_file !scm in
    let axiom = parse_axiom scm in
    let rules = parse_rules scm in
    let cycles = parse_cycles scm in
    let expressions = rewrite axiom rules cycles in
    if !print
    then
      if !print_ast
      then Printf.printf "Cycles: %d\nAST: %s\n" cycles (to_string_ast expressions)
      else Printf.printf "Cycles: %d\nGeneration: %s\n" cycles (to_string expressions)
    else (
      let actions = parse_actions scm in
      to_graphics 800 800 expressions actions)
;;
