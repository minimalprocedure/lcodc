(
  (axiom A)
  (cycles 1)
  (constants ())
  (variables (A B))
  (operators ([ ]))
  (rules ( 
    (B BB) 
    (A B[A]A) 
  ))
  (actions ( 
    (A ((label "a") (line_width 4) (color #2f422b) (forward 10) ) ) 
    (B ((label "b") (line_width 2) (color #2b201f) (forward 20)) )
    ([ ((storepush _) (left_by 45.0)) ) 
    (] ((storepop _) (right_by 45.0)) )  
  ))
)
