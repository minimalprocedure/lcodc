type scm_data =
  { axiom : string
  ; constants : string list
  ; variables : string list
  ; operators : string list
  ; rules : (string * string) list
  ; cycles : int option
  ; actions : (string * (string * string) list) list option
  }

type expression =
  | Op of string
  | Variable of string
  | Constant of string
  | Value of expression list

type rule = Rule of (expression * expression)

val parse_axiom : scm_data -> expression list
val parse_cycles : scm_data -> int
val parse_rules : scm_data -> rule list

val parse_actions
  :  scm_data
  -> (string * (CamelGraphics.Pos.t -> CamelGraphics.Pos.t) List.t) List.t

val rewrite : expression list -> rule list -> int -> expression list
val lsystem_from_file : string -> scm_data
val to_string : expression list -> string
val to_string_ast : expression list -> string

val to_graphics
  :  int
  -> int
  -> expression list
  -> (string * (CamelGraphics.Pos.t -> CamelGraphics.Pos.t) list) List.t
  -> unit
