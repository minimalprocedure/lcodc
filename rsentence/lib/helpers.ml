module Array_utils = struct
  let shuffle a =
    Array.iteri
      (fun i e ->
        let ri = Random.int (i + 1) in
        a.(i) <- a.(ri);
        a.(ri) <- e)
      a;
    a
  ;;
end

module File_utils = struct
  let lines_of_file
    ?(compact = false)
    ?(shuffle = false)
    ~(op : string -> string list)
    path
    =
    let ic = In_channel.open_text path in
    let readline () = In_channel.input_line ic in
    let rec loop acc =
      match readline () with
      | None ->
        In_channel.close ic;
        (*List.rev*) if shuffle then Array_utils.shuffle acc else acc
      | Some line -> loop (Array.append acc [| line |])
      (*line :: acc*)
    in
    let lines = loop [||] |> Array.to_list in
    if compact then [ op (String.concat " " lines) ] else List.map op lines
  ;;

  let write_file file text =
    let out = Out_channel.open_text file in
    Out_channel.output_string out text;
    Out_channel.close out
  ;;

  (*
  (* which one? maybe this is more slow then upper? *)
  let _lines_of_file ?(compact = false) ~(op : string -> string list) path =
    let ic = In_channel.open_text path in
    let text = In_channel.input_all ic in
    In_channel.close ic;
    if compact
    then [ op (Str.global_replace (Str.regexp "\n") " " text) ]
    else List.map (fun line -> op line) (String.split_on_char '\n' text)
  ;;
  *)

  (*
    Stream è deprecato da 4.14 e rimosso in 5.0
    Stream.from (fun _ ->
        try Some (input_line ic) with
        | End_of_file -> None)
  *)

  (*
  let stream_of_ch ic =
    Seq.of_dispenser (fun _ ->
      try Some (input_line ic) with
      | End_of_file -> None)
  ;;

  let lines_of_file ~op path =
    let ic = open_in path in
    let result = ref [] in
    try
      Seq.iter (fun data -> result := op data :: !result) (stream_of_ch ic);
      (*Stream.iter (fun data -> result := op data :: !result) (stream_of_ch ic);*)
      close_in ic;
      List.rev !result
    with
    | _ ->
      close_in ic;
      List.rev !result
  ;;
  *)
end

module List_utils = struct
  let split_rand l = List.partition (fun _ -> Random.bool ()) l

  let shuffle list =
    let rec shuff = function
      | [] -> []
      | [ e ] -> [ e ]
      | list ->
        let a, b = split_rand list in
        List.rev_append (shuff a) (shuff b)
    in
    shuff list
  ;;

  let range n = List.init n (fun x -> x + 1)

  let slices x y list =
    let rec take (left, center, right) i words =
      let i' = i + 1 in
      match words with
      | [] -> left, center, right
      | h :: t when i > y -> take (left, center, h :: right) i' t
      | h :: t when i >= x && i <= y -> take (left, h :: center, right) i' t
      | h :: t -> take (h :: left, center, right) i' t
    in
    let rev_triple tp =
      let left, center, right = tp in
      List.rev left, List.rev center, List.rev right
    in
    take ([], [], []) 0 list |> rev_triple
  ;;

  let slice x y list =
    let rec take i result = function
      | [] -> result
      | _ :: _ when i > y -> result
      | h :: t when i >= x && i <= y -> take (i + 1) (h :: result) t
      | _ :: t -> take (i + 1) result t
    in
    take 0 [] list |> List.rev
  ;;

  let conc l1 l2 =
    let l1' = List.rev l1 in
    let rec op acc l =
      match l with
      | [] -> acc
      | h :: t -> op (h :: acc) t
    in
    op l2 l1'
  ;;

  let ( @> ) = conc
  let last l = List.nth l (List.length l - 1)
  let rnd_int n = if n <= 0 then 0 else Random.int n
  let rnd_element list = List.nth list (rnd_int (List.length list - 1))

  let find_rnd_value values def =
    let frag_or_def v d =
      match v with
      | [ [] ] -> [ d ]
      | _ -> List.nth v 0
    in
    match values with
    | _ :: _ :: _ -> List.nth values (rnd_int (List.length values - 1))
    | _ as value -> frag_or_def value def
  ;;

  let push_assoc dict key value =
    match List.assoc_opt key dict with
    | None -> (key, [ value ]) :: dict
    | Some v ->
      let dict' = List.remove_assoc key dict in
      (key, [ value ] @ v) :: dict'
  ;;
end

module Hashtbl_utils = struct
  let find_rnd_key htbl =
    let open List in
    let keys = Hashtbl.fold (fun key _ keys -> key :: keys) htbl [] in
    let n = Random.int (length keys - 1) in
    nth keys n
  ;;

  let push ht key value =
    let op, value' =
      match Hashtbl.find_opt ht key with
      | None -> Hashtbl.add, [ value ]
      | Some v -> Hashtbl.replace, value :: v
    in
    op ht key value'
  ;;
end

module String_utils = struct
  let lines_of_string
    ?(compact = false)
    ?(shuffle = false)
    ~(op : string -> string list)
    text
    =
    let lines =
      if shuffle
      then List_utils.shuffle (Str.split (Str.regexp "\n") text)
      else Str.split (Str.regexp "\n") text
    in
    if compact
    then [ op (String.concat " " lines) ]
    else List.map (fun line -> op line) lines
  ;;

  (*String.split_on_char '\n' text*)

  let words ?transform s =
    let split = String.split_on_char ' ' in
    let filter = List.filter (fun c -> c <> "") in
    let items =
      match transform with
      | None -> split s
      | Some transform -> split (transform s)
    in
    filter items
  ;;
end

let to_chain n words =
  let open List_utils in
  let n' = n - 1 in
  let rec chain result x y ws =
    let sl = slice x y ws in
    match sl with
    | [] -> result
    | _ :: _ ->
      let value = slice (x + n) (y + n) ws in
      chain ((sl, value) :: result) (x + n) (y + n) ws
  in
  chain [] 0 n' words |> List.rev
;;

let dump_chain chain =
  let print pad l = Printf.sprintf "%s[%s]\n" pad (String.concat " | " l) in
  let print_key key = print "" key in
  let print_values values = List.map (print "  -> ") values |> String.concat "" in
  let seq_chain = Hashtbl.to_seq chain in
  Seq.fold_left
    (fun text (bag : string list * string list list) ->
      let key, values = bag in
      Printf.sprintf "%s%s%s" text (print_key key) (print_values values))
    ""
    seq_chain
;;

let get_only_stopped sentence =
  let m = Str.string_match (Str.regexp "^.*\\.") sentence 0 in
  if m then Str.matched_string sentence else sentence
;;
