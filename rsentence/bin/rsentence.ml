open Chs_generator

let model = ref ""
let chain_size = ref 2
let rec_limit = ref 100
let dump_chain = ref false
let word_case = ref 0
let view_full = ref false
let runs = ref 1
let model_compact = ref false
let shuffle_model = ref false
let verbose = ref false
let prealloc = ref 100

let usage =
  "rsentence usage: \n"
  ^ Sys.argv.(0)
  ^ "\n"
  ^ "   --model filename \n"
  ^ "   [--compact] \n"
  ^ "   [--shuffle] \n"
  ^ "   [--size number] \n"
  ^ "   [--limit number] \n"
  ^ "   [--case 0|1|2] \n"
  ^ "   [--dump] \n"
  ^ "   [--full ] \n"
  ^ "   [--run number] \n"
  ^ "   [--prealloc number] \n"
  ^ "   [--verbose] \n"
;;

let _args_parse =
  let opts =
    [ "--model", Arg.Set_string model, ": model file"
    ; "--compact", Arg.Set model_compact, ": model compact before process"
    ; "--shuffle", Arg.Set shuffle_model, ": shuffle model before process"
    ; "--size", Arg.Set_int chain_size, ": size (1,2 or 3 for best)"
    ; "--limit", Arg.Set_int rec_limit, ": recursion limit (100 default)"
    ; "--case", Arg.Set_int word_case, ": word case text"
    ; "--dump", Arg.Set dump_chain, ": dump flat chain"
    ; "--full", Arg.Set view_full, ": view full text with recusion limit indicator"
    ; "--run", Arg.Set_int runs, ": number of runs"
    ; "--prealloc", Arg.Set_int prealloc, ": prealloc hash"
    ; "--verbose", Arg.Set verbose, ": verbose processing"
    ]
  in
  let anons _ = () in
  Arg.parse opts anons usage
;;

let () =
  Random.self_init ();
  if Sys.file_exists !model
  then (
    let sentence =
      generate
        ~verbose:!verbose
        ~chain_size:!chain_size
        ~rec_limit:!rec_limit
        ~dump_chain:!dump_chain
        ~word_case:!word_case
        ~view_full:!view_full
        ~runs:!runs
        ~model_compact:!model_compact
        ~shuffle_model:!shuffle_model
        ~prealloc:!prealloc
        (ModelAsFile !model)
    in
    Printf.printf "%s\n" sentence)
  else Printf.printf "%s\n" usage
;;
