let hex_of_rgb (r, g, b, a) = Printf.sprintf "%.2x%.2x%.2x%.2x" r g b a
let hhex_of_rgb (r, g, b, a) = Printf.sprintf "#%s" (hex_of_rgb (r, g, b, a))
let cint_of_rgba (r, g, b, a) = (r lsl 24) + (g lsl 16) + (b lsl 8) + a
(*let image_height img = img.Image.height
let image_width img = img.Image.width*)

let rgba_of_hex hex =
  let value n = int_of_string (Printf.sprintf "0x%s" n) in
  try
    let m = Pcre.exec ~pat:{|([[:alnum:]]{2})((?1))((?1))((?1))|} hex in
    let color = Pcre.get_substrings ~full_match:false m in
    Some (value color.(0), value color.(1), value color.(2), value color.(3))
  with
  | Not_found -> None
;;

let split_every n str =
  let rec split acc from =
    try split (String.sub str from n :: acc) (from + n) with
    | Invalid_argument _ -> acc
  in
  split [] 0 |> List.rev
;;

let split_at pat str = Pcre.split ~pat str

module FLAT = struct
  type header =
    { w : int
    ; h : int
    ; t : string
    }

  let header_pixels data =
    try
      let m =
        Pcre.exec
          ~pat:{|^\[size=([0-9]+),([0-9]+)\s+type=([a-z]+)\]\n+([[:alnum:]\n]*)$|}
          data
      in
      let flat = Pcre.get_substrings ~full_match:false m in
      let pixels = split_at {|\n|} flat.(3) |> List.map (split_every 8) in
      { w = int_of_string flat.(0); h = int_of_string flat.(1); t = flat.(2) }, pixels
    with
    | Not_found -> failwith "FORMAT ERROR?"
  ;;

  let flat_of_pixel x y img =
    let fn r g b a = Printf.sprintf "%s" (hex_of_rgb (r, g, b, a)) in
    Image.read_rgba img x y fn
  ;;

  let flat_of_pixels row img =
    let indices = Helpers.range img.Image.width in
    let conv row index = flat_of_pixel index row img in
    let row = List.map (conv row) indices |> String.concat "" in
    Printf.sprintf "%s\n" row
  ;;

  let flat_of_img img =
    let w = img.Image.width in
    let h = img.Image.height in
    let rows = Helpers.range h in
    let conv r = flat_of_pixels r img in
    let himg = List.map conv rows |> String.concat "" in
    Printf.sprintf "[size=%d,%d type=rgba]\n%s" w h himg
  ;;

  let img_of_flat str =
    let { w; h; t }, pixels = header_pixels str in
    let is_alpha = if t = "rgba" then true else failwith "ONLY RGBA SCHEME!" in
    let img = Image.create_rgb ~alpha:is_alpha w h in
    let pixel h w hex =
      match rgba_of_hex hex with
      | Some (r, g, b, a) -> Image.write_rgba img w h r g b a
      | None -> failwith "NOT PIXEL COLOR"
    in
    List.iteri (fun h row -> List.iteri (pixel h) row) pixels;
    img
  ;;
end

module HTML = struct
  let html_of_pixel x y img =
    let fn r g b a =
      let hc = hhex_of_rgb (r, g, b, a) in
      Printf.sprintf "<i class=\"px\" style=\"background-color:%s\"></i>\n" hc
    in
    Image.read_rgba img x y fn
  ;;

  let html_of_pixels row img =
    let indices = Helpers.range img.Image.width in
    let conv row index = html_of_pixel index row img in
    let row = List.map (conv row) indices |> String.concat "" in
    Printf.sprintf "<div class=\"rw\">%s</div>\n" row
  ;;

  let html_header himg =
    Printf.sprintf
      "<!DOCTYPE html>
      <html>
        <head>
          <style>
          .rw {
            display: flex;
          }
          .px {
            width: 1px;
            height: 1px;
            display:block;
          }
          </style>
          <title></title>
          <link rel=\"stylesheet\" type=\"text/css\" href=\"custom.css\">
        </head>
        <body>
          %s
        </body>
      </html>"
      himg
  ;;

  let html_of_img img =
    let w = img.Image.width in
    let h = img.Image.height in
    let rows = Helpers.range h in
    let conv r = html_of_pixels r img in
    let himg = List.map conv rows |> String.concat "" in
    html_header
      (Printf.sprintf
         "<div data-w=\"%d\" data-h=\"%d\" class=\"image\">\n%s\n</div>\n"
         w
         h
         himg)
  ;;

  let hex_color_of_style style =
    try
      let m = Pcre.exec ~pat:{|background-color:\s*#([[:alnum:]]+)|} style in
      let color = Pcre.get_substrings ~full_match:false m in
      Some color.(0)
    with
    | Not_found -> None
  ;;

  open Soup

  let html_sizes html =
    let img = html $ "div.image" in
    ( int_of_string (img |> R.attribute "data-w")
    , int_of_string (img |> R.attribute "data-h") )
  ;;

  let img_of_html str =
    let html = parse str in
    let w, h = html_sizes html in
    let img = Image.create_rgb ~alpha:true w h in
    let rows = html $$ "div.rw" |> to_list in
    let pixel h w hpx =
      match R.attribute "style" hpx |> hex_color_of_style with
      | None -> failwith "NOT PIXEL COLOR"
      | Some hex ->
        (match rgba_of_hex hex with
        | None -> failwith "CONVERSION ERROR"
        | Some (r, g, b, a) -> Image.write_rgba img w h r g b a)
    in
    List.iteri
      (fun h row ->
        let pxs = row $$ "i.px" |> to_list in
        List.iteri (pixel h) pxs)
      rows;
    img
  ;;
end
