let () =
  print_endline "Convert image.png to image.html...";
  let img = FileHelpers.open_image "images/image.png" in
  let himg = Conv.HTML.html_of_img img in
  FileHelpers.save_text "images/image.html" himg;
  (*****************************************************************************)
  print_endline "Compress image.html to image.html.gz...";
  let chimg = Compression.deflate_string himg in
  FileHelpers.save_text "images/image.html.gz" chimg;
  (*****************************************************************************)
  print_endline "Convert image.html to image-conv.png...";
  let html = FileHelpers.read_file "images/image.html" in
  let img = Conv.HTML.img_of_html html in
  FileHelpers.save_image "images/image-html-conv.png" img;
  (*****************************************************************************)
  print_endline "Convert image.html.gz to image-html-gz-conv.png...";
  let htmlgz = FileHelpers.read_file "images/image.html.gz" in
  match Compression.inflate_string htmlgz with
  | Error _ -> failwith "ERROR INFLATE DATA"
  | Ok (_, html) ->
    (let img' = Conv.HTML.img_of_html html in
     FileHelpers.save_image "images/image-html-gz-conv.png" img');
  (*****************************************************************************)
  print_endline "Convert image.png to image.flat...";
  let timg = Conv.FLAT.flat_of_img img in
  FileHelpers.save_text "images/image.flat" timg;
  (*****************************************************************************)
  print_endline "Compress image.flat to image.flat.gz...";
  let ctimg = Compression.deflate_string timg in
  FileHelpers.save_text "images/image.flat.gz" ctimg;
  (*****************************************************************************)
  print_endline "Convert image.flat to image-flat-conv.png...";
  let flat = FileHelpers.read_file "images/image.flat" in
  let img = Conv.FLAT.img_of_flat flat in
  FileHelpers.save_image "images/image-flat-conv.png" img;
  (*****************************************************************************)
  print_endline "Convert image.flat.gz to image-flat-gz-conv.png...";
  let htmlgz = FileHelpers.read_file "images/image.flat.gz" in
  (match Compression.inflate_string htmlgz with
  | Error _ -> failwith "ERROR INFLATE DATA"
  | Ok (_, flat) ->
    (let img' = Conv.FLAT.img_of_flat flat in
      FileHelpers.save_image "images/image-flat-gz-conv.png" img');
    print_endline "done!")
;;
