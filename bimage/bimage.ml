open Graphics

let bytes_to_int_array bytes =
  Array.map (fun c -> Char.code c) (Bytes.to_seq bytes |> Array.of_seq)
;;

let get_bytes_from_file filename =
  let ic = open_in_bin filename in
  let size = in_channel_length ic in
  let buffer = Buffer.create size in
  (try Buffer.add_channel buffer ic size with
  | _ -> ());
  close_in ic;
  bytes_to_int_array (buffer |> Buffer.to_bytes)
;;

let arange a b = Array.init (b + 1 - a) (fun x -> x + a)

let array_slice_any n array =
  let alen = Array.length array in
  if n <= alen
  then (
    let num, rest = alen / n, alen mod n in
    let ret = Array.map (fun i -> Array.sub array (i * n) n) (arange 0 (num - 1)) in
    if rest = 0
    then ret
    else (
      let arest = Array.sub array (alen - rest) rest in
      Array.append ret [| arest |]))
  else raise (Invalid_argument "n > array lenght")
;;

let pads len aa =
  Array.map
    (fun r ->
      let len' = Array.length r in
      match len' with
      | _ when len' > len -> Array.sub r 0 len
      | _ when len' < len -> Array.append r (Array.make (len - len') 0)
      | _ -> r)
    aa
;;

let row_colors values =
  Array.map
    (fun c ->
      let l = Array.length c in
      match l with
      | 3 -> rgb c.(0) c.(1) c.(2)
      | 2 -> rgb c.(0) c.(1) 0
      | 1 -> rgb c.(0) 0 0
      | _ -> rgb 0 0 0)
    values
;;

let scale_row factor cells =
  Array.fold_left
    (fun acc cell ->
      let unt = Array.make factor cell in
      Array.append acc unt)
    [||]
    cells
;;

let scale_rows factor rows =
  Array.fold_left
    (fun acc row ->
      let unt = Array.make factor (scale_row factor row) in
      Array.append acc unt)
    [||]
    rows
;;

(*
let image_test =
  [| [| 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128 |]
   ; [| 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128 |]
   ; [| 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128 |]
   ; [| 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128 |]
   ; [| 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128 |]
   ; [| 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128 |]
   ; [| 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128 |]
   ; [| 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128 |]
   ; [| 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128 |]
   ; [| 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128 |]
   ; [| 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128 |]
   ; [| 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128 |]
   ; [| 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128 |]
   ; [| 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128 |]
   ; [| 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128 |]
   ; [| 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128; 128 |]
  |]
;;
*)

let path = ref ""
let resize = ref 2

let args_parse =
  let opts = [ "-r", Arg.Set_int resize, ": resize value" ] in
  let usage =
    "Read any file as image\nusage: " ^ Sys.argv.(0) ^ " path" ^ " -r resize value"
  in
  let anons x = path := x in
  Arg.parse opts anons usage
;;

let _ =
  let bytes = get_bytes_from_file !path in
  let colors = bytes |> array_slice_any 3 |> row_colors in
  let size = Float.sqrt (float_of_int (Array.length colors)) |> int_of_float in
  let aimage = colors |> array_slice_any size |> pads size |> scale_rows !resize in
  let w, h = Array.length aimage.(0), Array.length aimage in
  try
    open_graph (Printf.sprintf " %dx%d" w h);
    let image = make_image aimage in
    draw_image image 0 0;
    ignore (read_line ())
  with
  | _ -> ()
;;
