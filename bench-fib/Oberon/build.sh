#!/bin/bash
voc -M -OC -A88 Fib.obn
mv Fib fib-voc
rm Fib.c

obc -O -o fib-obc Fib.obn
rm Fib.k

obnc -o fib-obnc Fib.obn
rm .obnc -R

voc -M -OC -A88 FibIter.obn
mv FibIter fib-iter-voc
rm FibIter.c

obc -O -o fib-iter-obc FibIter.obn
rm FibIter.k

obnc -o fib-iter-obnc FibIter.obn
rm .obnc -R

voc -M -OC -A88 FibOpt.obn
mv FibOpt fib-opt-voc
rm FibOpt.c

obc -O -o fib-opt-obc FibOpt.obn
rm FibOpt.k

obnc -o fib-opt-obnc FibOpt.obn
rm .obnc -R