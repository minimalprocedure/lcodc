local C = terralib.includec("stdio.h")

local terra fib(n: int) : int 
  if (n <= 2)
    then return 1
    else return fib(n - 1) + fib(n - 2)
  end
end

local terra main()
    C.printf("%i\n", fib(45))
    return 0
end

return {
  main = main
}
