local Fib = require("fib")
local FibOpt = require("fib_opt")

local optimize = true
local arguments = {"-mtune=native", "-march=native", "-flto", "-fuse-ld=mold", "-s", "-static"}
local target = nil

terralib.saveobj("fib", {main = Fib.main}, arguments, target, optimize) 
terralib.saveobj("fib_opt", {main = FibOpt.main}, arguments, target, optimize) 
