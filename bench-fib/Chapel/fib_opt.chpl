proc fib(n : int) : int {
  proc rfib(n : int, a : int, b : int) : int {
    select(n) {
      when 0 do return a;
      when 1 do return b;
      otherwise do return rfib(n - 1, b, a + b);
    }
  }
  return rfib(n, 0, 1);
}

writeln(fib(45));
