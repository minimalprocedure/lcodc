proc fib(n : int) : int {
  if n <= 2 then return 1;
  return fib(n - 1) + fib(n - 2);
}

writeln(fib(45));