#!/bin/bash

exes=$(find . -type f -perm -o=x -type f ! -name '*.sh' -type f ! -name '*.dbg' -type f ! -name '*.so')
for exe in "${exes[@]}"; {
 	ls -sh $exe;
 	ldd $exe;
 	echo "-----------------------------------"; 
}
