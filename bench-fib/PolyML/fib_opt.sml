
fun fib n =
    let 
        fun rfib (0, a, b) = a
            | rfib (1, a, b) = b
	        | rfib (n, a, b) = rfib (n - 1, b, a + b)
    in
	    rfib (n, 0, 1)
    end

fun main () =
    print ((Int.toString (fib 45)) ^ "\n");

