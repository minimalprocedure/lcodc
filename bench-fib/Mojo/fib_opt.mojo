fn rfib(n: Int, a: Int, b: Int) -> Int:
    if n == 0:
        return a
    elif n == 1:
        return b
    else:
        return rfib(n - 1, b, a + b)

fn fib(n: Int) -> Int:
    return rfib(n, 0, 1)

fn main():
    print(fib(45))
