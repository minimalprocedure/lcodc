#include "share/atspre_staload.hats"

fun fib(n: int): int =
  let fun rfib(n: int, a: int, b: int): int =
    case n of
      | 0 => a
      | 1 => b
      | _ => rfib(n -1, b, a + b)
  in
  rfib(n, 0, 1)
end

implement
main0 () = println! (fib(45))