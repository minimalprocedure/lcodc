#include "share/atspre_staload.hats"

fun fib(n: int): int =
  if n <= 2 then 1 else fib(n - 1) + fib(n - 2)

implement
main0 () = println! (fib(45))