(defun fib (n)
  (if (< n 2) n (+ (fib (- n 2)) (fib (- n 1)))))

(defun main () (format t "~D~%" (fib 45)))