(defun fib_opt (n)
  (defun rfib (n a b)
    (case n
      (0 a)
      (1 b)
      (otherwise (rfib (- n 1) b (+ a b)))
    )
  )
  (rfib n 0 1)
)

(defun main () (format t "~D~%" (fib_opt 45)))