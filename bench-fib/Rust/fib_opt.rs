fn fib(n: u32) -> u32 {
  fn rfib(n: u32, a: u32, b: u32) -> u32 {
    match n {
      0 => a,
      1 => b,
      _ => rfib(n - 1, b, a + b)
    }    
  }
  return rfib(n, 0, 1);
}

fn main() {
  println!("{}", fib(45));
}