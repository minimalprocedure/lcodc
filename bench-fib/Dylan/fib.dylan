Module: fib
Synopsis: 
Author: 
Copyright: 

define method fib (n :: <integer>) => (n :: <integer>)
  select (n by \<=) 
    2 => 1;
    otherwise => fib (n - 1) + fib (n - 2);
  end
end method;

format-out("%d\n", fib(45));
