Module: dylan-user

define library fib_opt
  use common-dylan;
  use io;
end library fib_opt;

define module fib_opt
  use common-dylan;
  use format-out;
end module fib_opt;