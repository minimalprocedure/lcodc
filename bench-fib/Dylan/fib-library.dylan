Module: dylan-user

define library fib
  use common-dylan;
  use io;
end library fib;

define module fib
  use common-dylan;
  use format-out;
end module fib;
