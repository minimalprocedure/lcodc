Module: fib_opt
Synopsis: 
Author: 
Copyright: 

define method fib_opt (n :: <integer>) => (n :: <integer>)
  local method rfib 
    (n :: <integer>, a :: <integer>, b :: <integer>)
    select (n by \=) 
      0 => a;
      1 => b;
      otherwise => rfib (n - 1, b, a + b);
    end
  end;
  rfib (n, 0, 1);
end method;

format-out("%d\n", fib_opt(45));
