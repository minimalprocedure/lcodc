
Program Fib;

Function FIb(n: Cardinal): Cardinal;
Begin
  If n <= 0 Then Fib := 0
  Else If n <= 2 Then Fib := 1
  Else Fib := fib(n - 1) + fib(n - 2)
End;

Begin
  writeln(Fib(45));
End.
