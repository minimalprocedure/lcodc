
Program Fib;

Function FIb(n: Cardinal): Cardinal;

  Function rfib(n: Cardinal; a: Cardinal; b: Cardinal): Cardinal;
  Begin
    Case n Of 
      0: rfib := a;
      1: rfib := b;
      Else rfib := rfib(n - 1, b, a + b);
    End;
  End;

Begin
  If n <= 0 Then Fib := 0
  Else Fib := rfib(n, 0, 1)
End;

Begin
  writeln(Fib(45));
End.
