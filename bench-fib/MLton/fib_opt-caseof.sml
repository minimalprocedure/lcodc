
fun fib n =    
    let 
      fun rfib (n, a, b) = 
          case (n, a, b) of
            (0, a, b) => a
            | (1, a, b) => b
            | _ => rfib ((n - 1), b, (a + b))
    in
	    rfib (n, 0, 1)
    end

val _ =
    print ((Int.toString (fib 45)) ^ "\n");
