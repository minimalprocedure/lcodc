module fib_opt

import StdInt

fib_opt::Int -> Int
fib_opt n =
	let rfib a b n
		| n < 0 = 0
		| n == 0 = a
		| n == 1 = b
		| otherwise = rfib b (a + b) (n - 1)
	in
	rfib 0 1 n

Start::Int
Start = fib_opt 45
