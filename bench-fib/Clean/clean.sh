# Copyright (c) 2023 Massimo Ghisalberti
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

rm ./bin/*
rm ./nitrile-lock.json
rm ./nitrile-packages -Rvf 
rm "./src/Clean System Files" -Rvf 