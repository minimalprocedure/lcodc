(*
   Binet formula

   phi = (1.0 +. sqrt5) /. 2.0)
*)

let fib n = Float.pow 1.6180339887498949 n /. sqrt 5.0 |> int_of_float
let _ = Printf.printf "%d\n" (fib 45.0)
