let rec fib n =
  let rec mfib n store =
    if n <= 2
    then 1
    else (
      let n1 = n - 1 in
      let n2 = n - 2 in
      let m1 = Hashtbl.find_opt store n1 in
      let m2 = Hashtbl.find_opt store n2 in
      let v1 =
        match m1 with
        | Some v -> v
        | None ->
          let v = mfib n1 store in
          Hashtbl.add store n1 v;
          v
      and v2 =
        match m2 with
        | Some v -> v
        | None ->
          let v = mfib n2 store in
          Hashtbl.add store n2 v;
          v
      in
      v1 + v2)
  in
  mfib n (Hashtbl.create n)
;;

let _ = Printf.printf "%d\n" (fib 45)
