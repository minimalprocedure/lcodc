(*
   * F(2n) = F(n) * (2*F(n+1) - F(n))
   * F(2n+1) = F(n+1)^2 + F(n)^2
*)

(*
   it's more fast -> (1 lsl i) land n != 0 then mod?
*)

let leading_zero n =
  if n = 0
  then Sys.int_size
  else (
    let rec count zs n' = 
    if n' < 0 
    then zs 
    else count (zs + 1) (n' lsl 1) in
    count 0 n)
;;

let fib n =
  let rec calc i a b =
    match i with
    | -1 -> a
    | _ ->
    	Printf.printf "i: %d a: %d - b: %d\n" i a b;
      let a' = a * ((b lsl 1) - a) in
      let b' = (a * a) + (b * b) in
      Printf.printf "i: %d a': %d - b': %d\n" (i - 1) a' b';
      if (1 lsl i) land n != 0 
      then calc (i - 1) b' (a' + b') 
      else calc (i - 1) a' b' 
  in
  calc (Sys.int_size - leading_zero n) 0 1
;;

let _ = Printf.printf "%d\n" (fib 45)
