let fib n =
  let rec rfib = fun a b -> function
    | 0 -> a
    | 1 -> b
    | n when n < 0 -> failwith "n < 0"
    | n -> rfib b (a + b) (n - 1)
  in
  rfib 0 1 n
;;

let _ = Printf.printf "%d\n" (fib 45)
