#include <stdio.h>

int rfib(int n, int a, int b) {
  switch (n) {
  case 0:
    return a;
  case 1:
    return b;
  default:
    return rfib(n - 1, b, a + b);
  }
}

int fib(int n) { return rfib(n, 0, 1); }

int main(int argc, char *argv[]) { printf("%d\n", fib(45)); }
