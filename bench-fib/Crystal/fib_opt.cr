def fib(n : Int32)
  fn = uninitialized Proc(Int32, Int32, Int32, Int32)
  fn = ->(n : Int32, a : Int32, b : Int32) {
    case n
    when 0
      a
    when 1
      b
    else
      fn.call(n - 1, b, a + b)
    end
  }
  fn.call(n, 0, 1)
end

puts fib(45)
