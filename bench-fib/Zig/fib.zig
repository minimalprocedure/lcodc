const std = @import("std");

fn fib(n: u64) u64 {
    if (n <= 2) return 1;
    return fib(n - 1) + fib(n - 2);
}

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    try stdout.print("{}\n", .{fib(45)});
}
