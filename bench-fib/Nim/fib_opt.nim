
proc fib (n: int): int =
  proc rfib (n: int, a: int, b: int): int =
    case n:
    of 0: a
    of 1: b
    else: rfib(n - 1, b, a+b)
  result = rfib(n, 0, 1)

stdout.writeLine (fib(45))
