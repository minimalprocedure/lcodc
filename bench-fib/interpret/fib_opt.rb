# Copyright (c) 2023 Massimo Ghisalberti
#
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

def fib(n)
  def fn(n, a, b)
    case n
    when 0
      a
    when 1
      b
    else
      fn(n - 1, b, a + b)
    end
  end
  fn(n, 0, 1)
end

p fib(45)
