// Copyright (c) 2023 Massimo Ghisalberti
// 
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

const fib = (n) => n <= 2 ? 1 : fib(n - 1) + fib (n - 2);
console.log(fib(45));
