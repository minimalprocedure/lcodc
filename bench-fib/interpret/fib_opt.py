# Copyright (c) 2023 Massimo Ghisalberti
#
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

def fib(n):
    def fn(n, a, b):
        match n:
            case 0:
                return a
            case 1:
                return b
            case _:
                return fn(n - 1, b, a + b)
    return fn(n, 0, 1)


print(fib(45))
