// Copyright (c) 2023 Massimo Ghisalberti
// 
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

function fib(n) {
  const fn = (n, a, b) => {
    switch (n) {
      case 0:
        return a;
      case 1:
        return b;
      default:
        return fn(n - 1, b, a + b);
    }
  }
  return fn(n, 0, 1)
}

console.log(fib(45))