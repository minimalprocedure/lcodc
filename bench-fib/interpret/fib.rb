# Copyright (c) 2023 Massimo Ghisalberti
# 
# This software is released under the MIT License.
# https://opensource.org/licenses/MIT

def fib(n)
  if n <= 2
    1
  else
    fib(n - 1) + fib(n - 2)
  end
end

puts fib(45)