:- module fib_memo.

:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.

:- implementation.
:- import_module int, list, string.

main(!IO) :-
    io.format("%d\n", [i(fib(45))], !IO).

:- pragma memo(fib/1).  
:- func fib(int) = int.
fib(N) = R :-
    ( if N =< 2
    then R = 1
    else R = fib(N - 1) + fib(N - 2)
    ).
