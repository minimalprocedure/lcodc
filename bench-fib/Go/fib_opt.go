package main

import "fmt"

func fib(n int) int {
	var rfib func(n int, a int, b int) int
	rfib = func(n int, a int, b int) int {
		switch n {
		case 0:
			return a
		case 1:
			return b
		default:
			return rfib(n-1, b, a+b)
		}
	}
	return rfib(n, 0, 1)
}

func main() {
	fmt.Println(fib(45))
}
