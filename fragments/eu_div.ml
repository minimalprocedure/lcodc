
let div n d =
  let rec op acc n d =
    let res, ret = acc in
    if n < d then (res, n)
    else op (res + 1, 0) (n - d) d
  in
  op (0, 0) n d
