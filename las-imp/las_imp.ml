let look_and_say_imp d =
  let str = string_of_int d in
  let result = ref "" in
  let prev = ref str.[0] in
  let count = ref 0 in
  String.(
    iter
      (fun c ->
        if c = !prev
        then incr count
        else (
          result := !result ^ Printf.sprintf "%d%c" !count !prev;
          prev := c;
          count := 1))
      str);
  result := !result ^ Printf.sprintf "%d%c" !count !prev;
  int_of_string !result
;;

let gen_look_and_say_imp d l =
  Printf.printf "%d\n" d;
  let result = ref d in
  for _ = 0 to l do
    result := look_and_say_imp !result;
    Printf.printf "%d\n" !result;
  done
;;
