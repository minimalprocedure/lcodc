(*******************************************************************************
   application: Levenshtein - levenshtein distance calculator
   module: levenshtein.ml
   version: 1
   year: 2017
   developer: Massimo Ghisalberti
   email: zairik@gmail.com
   licence: GPL3
 ******************************************************************************)

let time f =
  let start = Unix.gettimeofday () in
  let x = f () in
  let stop = Unix.gettimeofday () in
  Printf.printf "Execution time: %fs\n%!" (stop -. start);
  x
;;

let minimum a b c = min a (min b c)

let memoize ?init fn i j =
  let memoized funct init =
    let table = Hashtbl.create init in
    fun i j ->
      try Hashtbl.find table (i, j) with
      | Not_found ->
        let result = funct i j in
        Hashtbl.add table (i, j) result;
        result
  in
  let init =
    match init with
    | None -> 0
    | Some x -> x
  in
  let fn_memoized_ref = ref (fun _ -> assert false) in
  let fn_memoized = memoized (fun i j -> fn !fn_memoized_ref i j) init in
  fn_memoized_ref := fn_memoized;
  fn_memoized i j
;;

let lev_slow s t =
  let cost i j = if s.[i - 1] = t.[j - 1] then 0 else 1 in
  let rec distance i j =
    match i, j with
    | i, 0 -> i
    | 0, j -> j
    | i, j ->
      let deletion = distance (i - 1) j + 1 in
      let insertion = distance i (j - 1) + 1 in
      let substitution = distance (i - 1) (j - 1) + cost i j in
      minimum deletion insertion substitution
  in
  distance (String.length s) (String.length t)
;;

let lev s t =
  let rec distance i j =
    match i, j with
    | i, 0 -> i
    | 0, j -> j
    | i, j when s.[i - 1] = t.[j - 1] -> distance (i - 1) (j - 1)
    | i, j ->
      1 + minimum (distance (i - 1) j) (distance i (j - 1)) (distance (i - 1) (j - 1))
  in
  distance (String.length s) (String.length t)
;;

let lev_memo_slow s t =
  let cost i j = if s.[i - 1] = t.[j - 1] then 0 else 1 in
  let distance fn i j =
    match i, j with
    | i, 0 -> i
    | 0, j -> j
    | i, j ->
      let deletion = fn (i - 1) j + 1 in
      let insertion = fn i (j - 1) + 1 in
      let substitution = fn (i - 1) (j - 1) + cost i j in
      minimum deletion insertion substitution
  in
  let ls = String.length s in
  let lt = String.length t in
  (memoize distance) ls lt
;;

let lev_memo s t =
  let distance fn i j =
    match i, j with
    | i, 0 -> i
    | 0, j -> j
    | i, j when s.[i - 1] = t.[j - 1] -> fn (i - 1) (j - 1)
    | i, j -> 1 + minimum (fn (i - 1) j) (fn i (j - 1)) (fn (i - 1) (j - 1))
  in
  let ls = String.length s in
  let lt = String.length t in
  (memoize distance) ls lt
;;

let memo = ref false
let slow = ref false
let bench = ref false
let string = ref ""
let target = ref ""

let args_parse =
  let opts =
    [ ( "-c"
      , Arg.Tuple [ Arg.Set_string string; Arg.Set_string target ]
      , ": comparable strings" )
    ; "-slow", Arg.Set slow, ": Set slow version"
    ; "-memo", Arg.Set memo, ": Set memoized version"
    ; "-time", Arg.Set bench, ": active benchmark"
    ]
  in
  let usage =
    "Levenshtein distance\nusage: "
    ^ Sys.argv.(0)
    ^ " [-c string target]"
    ^ " [-slow (slow version)]"
    ^ " [-memo (memoized version)]"
    ^ " [-time (active benchmark)]"
  in
  let anons _ = () in
  Arg.parse opts anons usage
;;

let () =
  let result =
    let fname, fn =
      match !slow with
      | true -> if !memo then "lev_memo_slow", lev_memo_slow else "lev_slow", lev_slow
      | false -> if !memo then "lev_memo", lev_memo else "lev", lev
    in
    if !bench
    then
      time (fun () ->
          Printf.printf "Function: %s - " fname;
          fn !string !target)
    else fn !string !target
  in
  Printf.printf "%d\n" result
;;
