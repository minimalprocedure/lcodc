open Ycomb

let fib_norec fn = function
  | 0 -> 0
  | 1 -> 1
  | n when n < 0 -> failwith "n < 0"
  | n -> fn (n - 1) + fn (n - 2)
;;

let rec fib_rec = function
  | 0 -> 0
  | 1 -> 1
  | n when n < 0 -> failwith "n < 0"
  | n -> fib_rec (n - 1) + fib_rec (n - 2)
;;

let fib_tail n =
  let rec rfib a b = function
    | 0 -> a
    | 1 -> b
    | n when n < 0 -> failwith "n < 0"
    | n -> rfib b (a + b) (n - 1)
  in
  rfib 0 1 n
;;

let fib_y_memo n = y (Memoization.memoizey fib_norec) n
let fib_y n = y fib_norec n
let fib_memo n = (Memoization.memoize1 fib_norec) n

(*
* F(2n) = F(n) * (2*F(n+1) - F(n))
* F(2n+1) = F(n+1)^2 + F(n)^2
*)

(*
 it's more fast -> (1 lsl i) land n != 0 then mod?
*)

let leading_zero n =
  if n = 0
  then Sys.int_size
  else (
    let rec count zs n' = if n' < 0 then zs else count (zs + 1) (n' lsl 1) in
    count 0 n)
;;

let fib_bit n =
  if n < 0
  then failwith "n < 0"
  else (
    let rec calc i a b =
      match i with
      | -1 -> a
      | _ ->
        let a' = a * ((b lsl 1) - a) in
        let b' = (a * a) + (b * b) in
        if (1 lsl i) land n != 0 then calc (i - 1) b' (a' + b') else calc (i - 1) a' b'
    in
    calc (Sys.int_size - leading_zero n) 0 1)
;;

(*
  Binet formula
  phi = (1.0 +. sqrt5) /. 2.0)
*)

let fib_binet n =
  if n < 0
  then failwith "n < 0"
  else (
    let n = float_of_int n in
    let sqrt5 = sqrt 5.0 in
    1.0
    /. sqrt5
    *. (Float.pow ((1.0 +. sqrt5) /. 2.0) n -. Float.pow ((1.0 -. sqrt5) /. 2.0) n)
    |> int_of_float)
;;

let fib_binet_precalc n =
  if n < 0
  then failwith "n < 0"
  else (
    let n = float_of_int n in
    Float.pow 1.6180339887498949 n /. sqrt 5.0 |> int_of_float)
;;
