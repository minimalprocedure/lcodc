(**
 * Copyright (c) 2023 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

 let nseconds = 1_000_000_000.00

let time f =
  let start = Unix.gettimeofday () in
  let x = f () in
  let stop = Unix.gettimeofday () in
  Printf.printf "Time: %fs%!" (stop -. start);
  x
;;

let time_n f =
  let start = Mtime_clock.now_ns () in
  let x = f () in
  let stop = Mtime_clock.now_ns () in
  let v = Int64.sub stop start in
  let seconds = (Int64.to_float v) /. nseconds in
  Printf.printf "Time: %.9fs%!" seconds;
  x
;;
