(**
 * Copyright (c) 2023 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

type 'a b = Roll of ('a b -> 'a)

let unroll (Roll x) = x
let y f = (fun g -> g (Roll g)) (fun x n -> f (unroll x x) n)
