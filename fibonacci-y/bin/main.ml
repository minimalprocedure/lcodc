open Fibonacci.Fib
open Fibonacci.Timers

let () =
  let num1 = 40 in
  let num2 = 40 in
  let num3 = 71 in
  let ret1 = time_n (fun () -> fib_y num1) in
  Printf.printf " -> fib_y (%d): %d\n%!" num1 ret1;
  let ret2 = time_n (fun () -> fib_rec num2) in
  Printf.printf " -> fib_rec (%d): %d\n%!" num2 ret2;
  let ret3 = time_n (fun () -> fib_y_memo num3) in
  Printf.printf " -> fib_y_memo (%d): %d\n%!" num3 ret3;
  let ret4 = time_n (fun () -> fib_memo num3) in
  Printf.printf " -> fib_memo (%d): %d\n%!" num3 ret4;
  let ret5 = time_n (fun () -> fib_tail num3) in
  Printf.printf " -> fib_tail (%d): %d\n%!" num3 ret5;
  let ret6 = time_n (fun () -> fib_bit num3) in
  Printf.printf " -> fib_bit (%d): %d\n%!" num3 ret6;

  (* 0 - 71 *)
  let ret7 = time_n (fun () -> fib_binet num3) in
  Printf.printf " -> fib_binet (%d): %d\n%!" num3 ret7;
  let ret8 = time_n (fun () -> fib_binet_precalc num3) in
  Printf.printf " -> fib_binet_precalc (%d): %d\n%!" num3 ret8
;;
