(** * Copyright (c) 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

let () =
  List.iter
    (fun n -> Printf.printf "%d - %s\n%!" n (RomNums.to_roman n))
    (List.init 100 (fun n -> n + 1))
;;
