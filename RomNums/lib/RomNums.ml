(** * Copyright (c) 2024 Massimo Ghisalberti
    *
    * This software is released under the MIT License.
    * https://opensource.org/licenses/MIT *)

(*
   let romans_arabics =
   [ "I", 1
  ; "V", 5
  ; "X", 10
  ; "L", 50
  ; "C", 100
  ; "D", 500
  ; "M", 1000
  ; "Ɔ", 500
  ; "ↀ", 1000
  ; "ↁ", 5000
  ; "ↂ", 10000
  ]
   ;;
*)

let ones = [| ""; "I"; "II"; "III"; "IV"; "V"; "VI"; "VII"; "VIII"; "IX" |]
let tens = [| ""; "X"; "XX"; "XXX"; "XL"; "L"; "LX"; "LXX"; "LXXX"; "XC" |]
let hundreds = [| ""; "C"; "CC"; "CCC"; "CD"; "D"; "DC"; "DCC"; "DCCC"; "CM" |]
let thousands = [| ""; "M"; "MM"; "MMM" |]

let to_roman num =
  let th = thousands.(num / 1000) in
  let h = hundreds.(num mod 1000 / 100) in
  let te = tens.(num mod 100 / 10) in
  let o = ones.(num mod 10) in
  Printf.sprintf "%s%s%s%s" th h te o |> String.trim
;;

let to_century ?(bc = "(bc)") ?(ac = "(ac)") num =
  let roman = to_roman (abs num) in
  if num > 0
  then Printf.sprintf "%s %s" roman ac
  else Printf.sprintf "%s %s" roman bc |> String.trim
;;
