open Ngui

let _ =
  let appl =
    new application
      WData.{ id = "application"; point = { x = 0; y = 0 }; width = 326; height = 150 }
  and edit =
    new edit WData.{ id = "edit"; point = { x = 10; y = 80 }; width = 304; height = 50 }
  and button1 =
    new button
      WData.{ id = "button"; point = { x = 10; y = 10 }; width = 100; height = 50 }
  and button2 =
    new button
      WData.{ id = "button"; point = { x = 112; y = 10 }; width = 100; height = 50 }
  and buttonClose =
    new button
      WData.{ id = "button"; point = { x = 214; y = 10 }; width = 100; height = 50 }
  in
  button1#addEventHandler MouseButton (fun _ev -> edit#setText "click bottone 1");
  button2#addEventHandler MouseButton (fun _ev -> edit#setText "click bottone 2");
  buttonClose#addEventHandler MouseButton (fun _ev -> raise Exit);
  button1#setLabel "button 1";
  button2#setLabel "button 2";
  buttonClose#setLabel "Chiudi";
  appl#register button1#asWidget;
  appl#register button2#asWidget;
  appl#register buttonClose#asWidget;
  appl#register edit#asWidget;
  appl#setTitle "Real Naive GUI";
  appl#show ()
;;
