open Graphics

module Point = struct
  type t =
    { x : int
    ; y : int
    }
end

module WData = struct
  type t =
    { point : Point.t
    ; width : int
    ; height : int
    ; id : string
    }
end

type eventType =
  | MouseMove
  | MouseButton
  | Key
  | MouseButtonAndKey

type synEvent =
  { evStatus : status
  ; evType : eventType
  ; targetData : WData.t
  }

type eventHandler = synEvent -> unit

class virtual widgetEvent =
  object (self)
    val handlers = Hashtbl.create 5

    method eventHandlers (evType : eventType) = Hashtbl.find_opt handlers evType

    method addEventHandler (evType : eventType) (callback : eventHandler) =
      match self#eventHandlers evType with
      | None -> Hashtbl.add handlers evType [ callback ]
      | Some callbacks -> Hashtbl.replace handlers evType (callbacks @ [ callback ])
  end

class virtual widget (wdata : WData.t) =
  object (self)
    inherit widgetEvent

    val data = wdata

    method data () = data

    (*method virtual asSuper : unit -> widget *)
    method asWidget = (self :> widget)

    method virtual draw : unit -> unit
  end

class button (wdata : WData.t) =
  object (self)
    inherit widget wdata

    val foreground = rgb 0 0 0

    val background1 = rgb 180 180 180

    val background2 = rgb 128 128 128

    val mutable label = ""

    method setLabel l = label <- l

    method drawWidget =
      let x1 = data.point.x + 8
      and y1 = data.point.y + 8
      and w1 = data.width - 16
      and h1 = data.height - 16 in
      set_color background1;
      fill_rect data.point.x data.point.y data.width data.height;
      set_color background2;
      fill_rect x1 y1 w1 h1;
      set_color foreground;
      draw_rect data.point.x data.point.y data.width data.height;
      draw_rect x1 y1 w1 h1

    method drawLabel =
      let ws, hs = text_size label in
      let pad_w = (data.width - ws) / 2 in
      let pad_h = (data.height - hs) / 2 in
      moveto (data.point.x + pad_w) (data.point.y + pad_h);
      draw_string label

    method draw () =
      self#drawWidget;
      self#drawLabel
  end

class edit (wdata : WData.t) =
  object (self)
    inherit widget wdata

    val mutable text = " "

    val mutable cursorPos = Point.{ x = 0; y = 0 }

    val foreground = rgb 0 0 0

    val background = rgb 200 200 200

    (*method private updateCursorPos () =
      cursorPos <- { x = current_x (); y = current_y () }*)
    method private updateText chr = text <- text ^ String.make 1 chr

    method setText s =
      text <- s;
      self#draw ()

    method draw () =
      let x = data.point.x
      and y = data.point.y
      and w = data.width
      and h = data.height in
      set_color background;
      fill_rect x y w h;
      moveto cursorPos.x cursorPos.y;
      set_color foreground;
      draw_rect x y w h;
      draw_string text

    initializer
    let w, h = text_size text in
    let pad_w = w
    and pad_h = (data.height - h) / 2 in
    let x = data.point.x + pad_w
    and y = data.point.y + pad_h in
    cursorPos <- { x; y };
    self#addEventHandler MouseButton (fun _ev -> moveto cursorPos.x cursorPos.y);
    self#addEventHandler Key (fun ev ->
        let _ =
          match ev.evStatus.key with
          | '\b' ->
            (* backspace *)
            let size = String.length text in
            if size > 0 then text <- String.sub text 0 (size - 1) else ()
          | _ -> self#updateText ev.evStatus.key
        in
        self#draw ())
  end

class application (wdata : WData.t) =
  object (self)
    val data = wdata

    val mutable widgets (*: 'widget list*) = []

    method register widget = widgets <- widget :: widgets

    method private draw () = List.iter (fun w -> w#draw ()) widgets

    method private isIntoWidget evStatus (data : WData.t) =
      evStatus.mouse_x >= data.point.x
      && evStatus.mouse_x <= data.point.x + data.width
      && evStatus.mouse_y >= data.point.y
      && evStatus.mouse_y <= data.point.y + data.height

    method setTitle caption = set_window_title caption

    method private dispatchEvents evStatus =
      let dispatch evStatus (w : widget) =
        let data = w#data () in
        if self#isIntoWidget evStatus data
        then (
          let t =
            match evStatus.button, evStatus.keypressed with
            | true, false -> MouseButton
            | false, true -> Key
            | true, true -> MouseButtonAndKey
            | _ -> MouseMove
          in
          match w#eventHandlers t with
          | None -> ()
          | Some callbacks ->
            List.iter
              (fun callback -> callback { evStatus; evType = t; targetData = data })
              callbacks)
        else ()
      in
      List.iter (dispatch evStatus) widgets

    method show () =
      self#draw ();
      try
        while true do
          let st = wait_next_event [ Mouse_motion; Button_down; Key_pressed ] in
          self#dispatchEvents st
        done
      with
      | Exit -> ()

    initializer open_graph (Printf.sprintf " %dx%d" data.width data.height)
  end
