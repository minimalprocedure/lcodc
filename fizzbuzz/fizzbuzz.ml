type gamenum =
  | Normal of int
  | Fizz of int
  | Buzz of int
  | FizzBuzz of int

let is_div_by d n = n mod d = 0
let is_div_by3 = is_div_by 3
let is_div_by5 = is_div_by 5

let gamenum n =
  match n with
  | n when is_div_by3 n && is_div_by5 n -> FizzBuzz n
  | n when is_div_by3 n -> Fizz n
  | n when is_div_by5 n -> Buzz n
  | _ -> Normal n
;;

let print ?boo n =
  let s =
    match boo with
    | None -> ""
    | Some s -> s
  in
  Printf.printf "%d %s\n" n s
;;

let print_gamenum gn =
  match gn with
  | Normal n -> print n
  | Fizz n -> print n ~boo:"Fizz"
  | Buzz n -> print n ~boo:"Buzz"
  | FizzBuzz n -> print n ~boo:"FizzBuzz"
;;

let fizzbuzz ~outf min max =
  let rec fn curr =
    if curr > max
    then curr - 1
    else (
      let () = outf (gamenum curr) in
      fn (curr + 1))
  in
  fn min
;;

let _ = fizzbuzz 1 15 ~outf:print_gamenum
