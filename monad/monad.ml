module type Monad = sig
  type 'a t

  val return : 'a -> 'a t
  val bind : 'a t -> ('a -> 'b t) -> 'b t
end

module OptionMonad = struct
  type 'a t = 'a option

  let return x = Some x

  let bind m op =
    match m with
    | None -> None
    | Some x -> op x
  ;;

  let ( >>= ) = bind

  let ( >|= ) m op =
    match m with
    | None -> None
    | Some x -> return (op x)
  ;;

  let compose op op2 x = op x >>= fun y -> op2 y
  let ( >=> ) = compose

  let ( >>|= ) m op =
    match m with
    | None -> None
    | Some x ->
      op x;
      return x
  ;;
end

open OptionMonad

let process x =
  Some x
  >>|= (fun n -> Printf.printf "%d\n" n)
  >>= (fun n -> return (n + 1))
  >>= (fun n -> return (n + 1))
  >>|= (fun n -> Printf.printf "%d\n" n)
  >|= (fun n -> n + 1)
  >>|= (fun n -> Printf.printf "%d\n" n)
  >>= fun n ->
  return (n + 1)
  >>|= (fun n -> Printf.printf "%d\n" n)
  >>= fun n ->
  return (n + 1)
  >>|= (fun n -> Printf.printf "%d\n" n)
  >>= fun n ->
  return (string_of_int n)
  >>|= (fun s -> Printf.printf "n is string %s\n" s)
  >>= fun s -> return (int_of_string s) >>|= fun n -> Printf.printf "n is int %d\n" n
;;

let () = ignore (process 1)
