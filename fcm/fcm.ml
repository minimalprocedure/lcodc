module PersonData = struct
  type t =
    { name : string
    ; lastname : string
    }
end

module type TPerson = sig
  val data : PersonData.t
  val get_fullname : unit -> string
end

module type TPersonData = sig
  val data : PersonData.t
end

module Person (Data : TPersonData) : TPerson = struct
  let data = PersonData.{ name = Data.data.name; lastname = Data.data.lastname }
  let get_fullname () = Printf.sprintf "%s %s" data.name data.lastname
end

let initPerson name lastname =
  let module X = struct
    let data = PersonData.{ name; lastname }
  end
  in
  (module X : TPersonData)
;;

module Pico = Person ((val initPerson "Pico" "De Paperis"))
module Paperino = Person ((val initPerson "Donald" "Duck"))
module Topolino = Person ((val initPerson "Mickey" "Mouse"))

let () =
  let persons =
    [ (module Topolino : TPerson); (module Pico : TPerson); (module Paperino : TPerson) ]
  in
  let print_fullname m =
    let module P = (val m : TPerson) in
    print_endline (P.get_fullname ())
  in
  List.iter print_fullname persons
;;
