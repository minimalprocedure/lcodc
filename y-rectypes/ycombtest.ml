module Phase1 = struct
  let rec fact n = if n = 0 then 1 else n * fact (n - 1)
end

module Phase2 = struct
  let rec fact n = if n = 0 then 1 else n * fact (n - 1)

  let fact () n = if n = 0 then 1 else n * fact (n - 1)

  let recFact = fact ()
end

module Phase3 = struct
  let rec fact n = if n = 0 then 1 else n * fact (n - 1)

  let fact recFn n = if n = 0 then 1 else n * recFn (n - 1)

  let rec recFn n = (fact recFn) n

  let recFact = fact recFn
end

module Phase4 = struct

  let fact fn = fun n -> if n = 0 then 1 else n * fn (n - 1)

  let recFn fn = fact (fun n -> fn fn n) (* -rectype flags*)

  let recFact = recFn recFn
end

module Phase5 = struct

  let fact fn = fun n -> if n = 0 then 1 else n * fn (n - 1)

  let recFn fn = fact (fun n -> fn fn n) (* -rectype flags*)

  let y () = (fun fn -> fn fn ) recFn (* -rectype flags*)

  let recFact = y ()
end

module Phase6 = struct

  let fact fn = fun n -> if n = 0 then 1 else n * fn (n - 1)

  let recFn fn = fact (fun n -> fn fn n) (* -rectype flags*)

  let y () = (fun fn -> fn fn ) (fun fn -> fact (fun n -> fn fn n)) (* -rectype flags*)

  let recFact = y ()
end

module Phase7 = struct

  let fact fn = fun n -> if n = 0 then 1 else n * fn (n - 1)

  let y toRecFn = (fun fn -> fn fn ) (fun fn -> toRecFn (fun n -> fn fn n) ) (* -rectype flags*)

  let recFact = y fact
end

let () =
  Printf.printf "Phase1: %d\n" (Phase1.fact 5);
  Printf.printf "Phase2: %d\n" (Phase2.recFact 5);
  Printf.printf "Phase3: %d\n" (Phase3.recFact 5);
  Printf.printf "Phase4: %d\n" (Phase4.recFact 5);
  Printf.printf "Phase5: %d\n" (Phase5.recFact 5);
  Printf.printf "Phase6: %d\n" (Phase6.recFact 5);
  Printf.printf "Phase7: %d\n" (Phase7.recFact 5);
;;
