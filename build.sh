#!/bin/bash

for d in $(ls -d */); do 
  if [ -f "$d/dune" ] && [ ! -f "$d/package.json" ] ; then
    proj=${d%%/}
    cd $d
    echo "Build $proj"
    dune build "$proj.exe"
    cd ..
  fi
done

