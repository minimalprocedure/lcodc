
function fact() {
  return (function (n) {
    return n == 0 ? 1 : n * fact(n - 1)
  })
}

const recFact = fact();

console.log(`Phase2: ${recFact()}`);

