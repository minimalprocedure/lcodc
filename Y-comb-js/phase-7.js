
function fact(fun) {
  return function (n) {
    return n == 0 ? 1 : n * fun(n - 1);
  };
}

function fib(fun) {
	return function (n) {
	   return n < 1 ? 0
      : n <= 2 ? 1
      : fun(n - 1) + fun(n - 2)
	}
}

function Y(toRecFn) {
  return (function (fun) {
    return fun(fun);
  })(function (fun) {
    return toRecFn(function (n) {
      return fun(fun)(n)
    })
  }
  );
}

const recFact = Y(fact);

const recFib = Y(fib);

console.log(`Phase7: ${recFact(5)}`);
console.log(`Phase7 fib: ${recFib(10)}`);
