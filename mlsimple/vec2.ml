(**
 * Copyright (c) 2021 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

let inner_op op value pair =
  let s =
    match pair with
    | None, Some v | Some v, None -> v
    | Some v1, Some v2 -> op v1 v2
    | _ -> 0.0
  in
  s :: value
;;

let vsum v1 v2 =
  let op = inner_op ( +. ) in
  List.rev (List.fold_left op [] (ListUtils.zip_opt v1 v2))
;;

let vdiff v1 v2 =
  let v2' = List.map ( ~-. ) v2 in
  vsum v1 v2'
;;

let prod s v =
  let mult x = x *. s in
  List.map mult v
;;

let dotprod v1 v2 =
  let op = inner_op ( *. ) in
  List.fold_left op [] (ListUtils.zip_opt v1 v2) |> List.rev |> List.fold_left ( +. ) 0.0
;;

let magnitude v1 =
  let op memo n = memo +. Float.pow n 2.0 in
  List.fold_left op 0.0 v1 |> Float.sqrt
;;

let magnitude2 v1 = Float.sqrt (dotprod v1 v1)

let cosine_similarity v1 v2 =
  let m1 = magnitude v1 in
  let m2 = magnitude v2 in
  let dp = dotprod v1 v2 in
  dp /. (m1 *. m2)
;;
