(**
 * Copyright (c) 2021 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

(* John Machin bapt. c. 1686 – June 9, 1751, Pi formula *)
let pi = 4.0 *. ((4.0 *. atan (1.0 /. 5.0)) -. atan (1.0 /. 239.0))

(* Leibniz formula for π *)
let pi2 = 4.0 *. atan 1.0
let degree_of_radian angle = angle *. 180.0 /. pi
let radian_of_degree angle = angle *. pi /. 180.0
let acosd c = degree_of_radian (acos c)
let cosd a = cos (radian_of_degree a)
