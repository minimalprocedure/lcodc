(**
 * Copyright (c) 2021 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

let words_split = Str.regexp "[ \n\t;,.'!?()<>¬’“”\"\\/&—\\-]+"
let vocs_split = Str.regexp "[ \n]+"
let lowercase_all words = List.map String.lowercase_ascii words
let term (t, _i, _v) = t
let index (_t, i, _v) = i
let term_index (t, i, _v) = t, i
let value (_t, _i, v) = v
let vector fn data = List.map fn data

let words ?(min = 2) str =
  let ws = Str.split words_split str in
  (if min > 0 then List.filter (fun s -> String.length s > min) ws else ws)
  |> lowercase_all
;;

type vocabulary =
  | VocList of string list
  | VocString of string

let vocabulary vocs =
  (match vocs with
  | VocList vs -> vs
  | VocString vs -> Str.split vocs_split vs)
  |> lowercase_all
  |> List.sort_uniq compare
  |> List.mapi (fun i t ->
         match String.split_on_char '=' t with
         | [ t; v ] -> t, i, float_of_string v
         | _ -> t, i, 1.0)
;;

let get_voc vocabulary term = List.find_opt (fun (t, _, _) -> t = term) vocabulary

let reduce_words vocabulary words =
  let get = get_voc vocabulary in
  List.fold_left
    (fun acc token ->
      match get token with
      | None -> acc
      | Some (t, i, _) -> (t, i) :: acc)
    []
    words
;;

type bow =
  | Count
  | Presence

let bow ?(t = Count) vocabulary words =
  let bag = Array.init (List.length vocabulary) (fun _ -> 0.0) in
  List.iter
    (fun (_, i) ->
      match t with
      | Count -> bag.(i) <- bag.(i) +. 1.0
      | Presence -> bag.(i) <- 1.0)
    (reduce_words vocabulary words);
  List.map (fun (t, i, _) -> t, i, bag.(i)) vocabulary
;;
