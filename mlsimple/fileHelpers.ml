(**
 * Copyright (c) 2021 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

let read_text_ch ic =
  let rec read content ic =
    try
      let content' = input_line ic :: content in
      read content' ic
    with
    | End_of_file -> Some (content |> List.rev |> String.concat "\n")
    | _ -> None
  in
  read [] ic
;;

let read_file path =
  let in_c = open_in path in
  let content = read_text_ch in_c in
  match content with
  | Some s -> s
  | None -> ""
;;

let write_text_to_file text filename =
  let out_file = open_out filename in
  Printf.fprintf out_file "%s" text;
  close_out out_file
;;

let explode str = List.of_seq (String.to_seq str)
let implode chars = String.of_seq (List.to_seq chars)
