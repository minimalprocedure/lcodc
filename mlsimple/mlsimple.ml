(**
 * Copyright (c) 2021 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

let () =
  print_endline "vocabolario voc.txt";
  print_endline "---------------------";
  let voc_str = FileHelpers.read_file "data/voc.txt" in
  let vocabulary = Text.vocabulary (Text.VocString voc_str) in
  let voc_vector = Text.vector Text.value vocabulary in
  Printf.printf "Magnitude vocabulary %f\n" (Vec2.magnitude voc_vector);
  print_endline "\nElaborazione vocabulary vs text1.txt";
  print_endline "---------------------";
  let text1 = FileHelpers.read_file "data/text1.txt" in
  let words1 = Text.words text1 in
  let bow1 = Text.bow vocabulary words1 ~t:Presence in
  let bow1_vector = Text.vector Text.value bow1 in
  let cos_sim1 = Vec2.cosine_similarity voc_vector bow1_vector in
  Printf.printf "Magnitude text1.txt %f\n" (Vec2.magnitude bow1_vector);
  Printf.printf "Cosine Similarity vocabulary - text1.txt: %f\n" cos_sim1;
  Printf.printf "Vector angle vocabulary - text1.txt: %f°\n" (MathUtils.acosd cos_sim1);
  print_endline "\nElaborazione vocabulary vs text2.txt";
  print_endline "---------------------";
  let text2 = FileHelpers.read_file "data/text2.txt" in
  let words2 = Text.words text2 in
  let bow2 = Text.bow vocabulary words2 ~t:Presence in
  let bow2_vector = Text.vector Text.value bow2 in
  let cos_sim2 = Vec2.cosine_similarity voc_vector bow2_vector in
  Printf.printf "Magnitude text2.txt %f\n" (Vec2.magnitude bow2_vector);
  Printf.printf "Cosine Similarity vocabulary - text2.txt: %f\n" cos_sim2;
  Printf.printf "Vector angle vocabulary - text2.txt: %f°\n" (MathUtils.acosd cos_sim2);
  print_endline "\nElaborazione text1.txt vs text2.txt";
  print_endline "---------------------";
  let cos_sim3 = Vec2.cosine_similarity bow1_vector bow2_vector in
  Printf.printf "Cosine Similarity text1.txt - text2.txt: %f\n" cos_sim3;
  Printf.printf "Vector angle text1.txt - text2.txt: %f°\n" (MathUtils.acosd cos_sim3)
;;
