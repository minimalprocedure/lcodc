let explode str = List.of_seq (String.to_seq str)
let implode chars = String.of_seq (List.to_seq chars)

let starts_with prefix chars =
  let rec scan_delim = function
    | [], chars -> Some chars
    | p :: prefix, c :: chars when p = c -> scan_delim (prefix, chars)
    | _ -> None
  in
  scan_delim (prefix, chars)
;;

let delimited start stop chars =
  let rec get_body acc chars =
    match starts_with stop chars, chars with
    | None, [] -> None
    | None, c :: rest -> get_body (c :: acc) rest
    | Some chars, _ -> Some (List.rev acc, chars)
  in
  match starts_with start chars with
  | None -> None
  | Some chars -> get_body [] chars
;;

let scan1 start stop chars =
  let rec parser spans acc chars =
    match delimited start stop chars, chars with
    | None, c :: rest -> parser spans (c :: acc) rest
    | None, [] -> List.rev spans
    | Some (cword, rest), _ ->
      let word = implode cword in
      parser (word :: spans) [] rest
  in
  parser [] [] chars
;;

let scan start stop chars =
  let impacc acc = implode (List.rev acc) in
  let rec parser spans acc chars =
    match delimited start stop chars, chars with
    | None, c :: rest -> parser spans (c :: acc) rest
    | None, [] ->
      let lit = impacc acc in
      List.rev (lit :: spans)
    | Some (cword, rest), _ ->
      let word = "<" ^ implode cword ^ ">" in
      let lit = impacc acc in
      parser (word :: lit :: spans) [] rest
  in
  parser [] [] chars
;;

let scan_apply ?op start stop chars =
  let impacc acc = implode (List.rev acc) in
  let rec parser spans acc chars =
    match delimited start stop chars, chars with
    | None, c :: rest -> parser spans (c :: acc) rest
    | None, [] -> List.rev (impacc acc :: spans)
    | Some (cword, rest), _ ->
      let key = implode cword in
      let value =
        match op with
        | Some op -> op key
        | None -> key
      in
      parser (value :: impacc acc :: spans) [] rest
  in
  parser [] [] chars
;;

let rstr tpl vars =
  let find key =
    match Hashtbl.find_opt vars key with
    | None -> Printf.sprintf "[key: <%s> not_found]" key
    | Some v -> v
  in
  let chars = explode tpl in
  scan_apply [ '#'; '{' ] [ '}' ] chars ~op:find |> String.concat ""
;;

let () =
  let vars = Hashtbl.create 2 in
  Hashtbl.add vars "name" "Oronzo";
  Hashtbl.add vars "lastname" "Chiappette";
  let str = rstr "Buongiorno #{name} #{lastname}!" vars in
  print_endline str
;;
