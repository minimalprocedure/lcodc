(**
 * Copyright (c) 2023 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

let fred = 0.2126 (* 0.299 *)
let fgreen = 0.7152 (* 0.587 *)
let fblue = 0.0722 (* 0.114 *)

let grayze ?(fred = 1.) ?(fgreen = 1.) ?(fblue = 1.) r g b =
  let gr = fred *. float_of_int r in
  let gg = fgreen *. float_of_int g in
  let gb = fblue *. float_of_int b in
  (gr +. gg +. gb |> Float.ceil |> int_of_float) mod 256
;;
