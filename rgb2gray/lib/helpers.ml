(**
 * Copyright (c) 2023 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

(*let range n = List.init (n - 1) (fun x -> x)
let time () = Int32.of_float (Unix.time ())*)

let uint8_to_bytes (n : int) =
  let b = Bytes.create 1 in
  Bytes.set_uint8 b 0 n;
  b
;;

let set_int32 = Bytes.set_int32_ne
let get_int32 = Bytes.get_int32_ne

let int32_to_bytes n =
  let b = Bytes.create 4 in
  set_int32 b 0 (Int32.of_int n);
  b
;;

let bytes_to_int32 = get_int32

let bytes_trunc_header bytes n =
  for i = 0 to n do
    Bytes.set bytes i ' '
  done;
  Bytes.trim bytes
;;
