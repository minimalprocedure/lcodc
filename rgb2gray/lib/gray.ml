(**
 * Copyright (c) 2023 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
 *)

type typeimage =
  | RgbA
  | GreyA

let process_channels ?(fred = 1.) ?(fgreen = 1.) ?(fblue = 1.) r g b typeimage =
  match typeimage with
  | RgbA ->
    let gr = fred *. float_of_int r in
    let gg = fgreen *. float_of_int g in
    let gb = fblue *. float_of_int b in
    int_of_float gr mod 256, int_of_float gg mod 256, int_of_float gb mod 256
  | GreyA ->
    let gray = Colors.grayze ~fred ~fgreen ~fblue r g b in
    gray, gray, gray
;;

let process ?colors ~fred ~fgreen ~fblue ~typeimage ~inflat img =
  let process_row row img typeimage =
    let apply row col r g b a =
      let r', g', b' =
        if inflat then r, g, b else process_channels ~fred ~fgreen ~fblue r g b typeimage
      in
      Image.write_rgba img col row r' g' b' a
    in
    for col = 0 to img.Image.width - 1 do
      Image.read_rgba img col row (apply row col)
    done
  in
  for row = 0 to img.Image.height - 1 do
    process_row row img typeimage
  done;
  match colors with
  | Some _ -> Quant.colors_reduce ?colors img
  | None -> img
;;

let open_image = ImageLib_unix.openfile

let save_image
  ?(typeimage = RgbA)
  ?(dump_colors = false)
  ?(fred = Colors.fred)
  ?(fgreen = Colors.fgreen)
  ?(fblue = Colors.fblue)
  ?(inflat = false)
  ?colors
  fname
  img
  =
  Quant.dump_colors_to_file ~suffix:".pre" ~dump_colors fname img;
  let img = process ?colors ~fred ~fgreen ~fblue ~typeimage ~inflat img in
  Quant.dump_colors_to_file ~suffix:".post" ~dump_colors fname img;
  ImageLib_unix.writefile fname img
;;
