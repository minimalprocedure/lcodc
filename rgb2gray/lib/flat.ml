(**
 * Copyright (c) 2023 Massimo Ghisalberti
 * 
 * This software is released under the MIT License.
 * https://opensource.org/licenses/MIT
  *)

type compressor =
  | Lz4
  | Zstd

let compressor_engine = Zstd

type typeimage =
  | WithAlpha
  | NoAlpha

type channels =
  | Red
  | Green
  | Blue
  | Alpha
  | Gray

let get_alpha_channel odata pos = function
  | WithAlpha -> Some (Bytes.get odata (pos + 1) |> int_of_char)
  | NoAlpha -> None
;;

let mult_by_type = function
  | WithAlpha -> 2, true
  | NoAlpha -> 1, false
;;

let compressZstd data =
  let output = Zstandard.Output.allocate_string ~size_limit:None in
  let input = Zstandard.Input.from_bytes data in
  Zstandard.Simple.compress ~compression_level:18 ~input ~output |> String.to_bytes
;;

let decompressZstd ~length data =
  let size_limit = Some length in
  let output = Zstandard.Output.allocate_string ~size_limit in
  let input = Zstandard.Input.from_bytes data in
  Zstandard.Simple.decompress ~input ~output |> String.to_bytes
;;

let compress data = function
  | Lz4 -> LZ4.Bytes.compress data
  | Zstd -> compressZstd data
;;

let decompress ~datasize data = function
  | Lz4 -> LZ4.Bytes.decompress ~length:datasize data
  | Zstd -> decompressZstd ~length:datasize data
;;

let prepend_flat_header width height data =
  let size = Bytes.length data in
  let buffer = Buffer.create (4 + 4 + size) in
  Buffer.add_bytes buffer (Helpers.int32_to_bytes width);
  Buffer.add_bytes buffer (Helpers.int32_to_bytes height);
  Buffer.add_bytes buffer data;
  Buffer.to_bytes buffer
;;

let add_alpha_channel buffer a = function
  | WithAlpha -> Buffer.add_bytes buffer (Helpers.uint8_to_bytes a)
  | NoAlpha -> ()
;;

let flatize
  ?(typeimage = WithAlpha)
  ?(channel = Green)
  ?(fred = 1.)
  ?(fgreen = 1.)
  ?(fblue = 1.)
  img
  =
  let height = img.Image.height in
  let width = img.Image.width in
  let mult, _ = mult_by_type typeimage in
  let buffer = Buffer.create (width * height * mult) in
  let add_to_buffer r g b a =
    let chc =
      match channel with
      | Red -> r
      | Green -> g
      | Blue -> b
      | Alpha -> a
      | Gray -> Colors.grayze ~fred ~fgreen ~fblue r g b
    in
    Buffer.add_bytes buffer (Helpers.uint8_to_bytes chc);
    add_alpha_channel buffer a typeimage
  in
  let rec process_data col row =
    let col = col mod width in
    let row = if col = 0 then row + 1 else row in
    if row < height
    then (
      Image.read_rgba img col row add_to_buffer;
      process_data (col + 1) row)
    else buffer
  in
  let buff = process_data 0 (-1) in
  let data = Buffer.to_bytes buff in
  let cdata = compress data compressor_engine in
  prepend_flat_header width height cdata
;;

let save_image
  ?(typeimage = WithAlpha)
  ?(dump_colors = false)
  ?(fred = 1.)
  ?(fgreen = 1.)
  ?(fblue = 1.)
  ?colors
  ~channel
  fname
  img
  =
  let oc = Out_channel.open_bin fname in
  Quant.dump_colors_to_file ~suffix:".pre" ~dump_colors fname img;
  let img =
    match colors with
    | Some _ ->
      let img = Quant.colors_reduce ?colors img in
      Quant.dump_colors_to_file ~suffix:".post" ~dump_colors fname img;
      img
    | None -> img
  in
  let data = flatize ~typeimage ~channel ~fred ~fgreen ~fblue img in
  Out_channel.output_bytes oc data;
  Out_channel.close oc
;;

let extract_header_data data =
  let width = Helpers.get_int32 data 0 |> Int32.to_int in
  let height = Helpers.get_int32 data 4 |> Int32.to_int in
  let cdata = Helpers.bytes_trunc_header data 7 in
  width, height, cdata
;;

let unflatize ?(typeimage = WithAlpha) data =
  let iwidth, iheight, cdata = extract_header_data data in
  let mult, alpha = mult_by_type typeimage in
  let datasize = iwidth * iheight * mult in
  let odata = decompress ~datasize cdata compressor_engine in
  let decode odata datasize width height =
    let img = Image.create_rgb ~alpha width height in
    let rec decode_pixel col row pos =
      let row, col =
        if (pos mod (width * mult)) - 1 < 0 then row + 1, 0 else row, col + 1
      in
      if pos < datasize
      then (
        let c = Bytes.get odata pos |> int_of_char in
        let _ =
          match get_alpha_channel odata pos typeimage with
          | Some a -> Image.write_rgba img col row c c c a
          | None -> Image.write_rgb img col row c c c
        in
        ();
        decode_pixel col row (pos + mult))
      else img
    in
    decode_pixel 0 (-1) 0
  in
  decode odata datasize iwidth iheight
;;

let open_image ?(typeimage = WithAlpha) fname =
  let ic = In_channel.open_bin fname in
  let indata = In_channel.input_all ic |> String.to_bytes in
  In_channel.close ic;
  unflatize ~typeimage indata
;;
