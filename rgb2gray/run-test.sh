#!/bin/bash
dune build

rm ./images/image-color.png
rm ./images/image-color-64.png
rm ./images/image-gray.flat
rm ./images/image-gray.flat.png
rm ./images/image-gray.png
rm ./images/image-gray-8.flat
rm ./images/image-gray-8.flat.png
rm ./images/image-gray-16.flat.png
rm ./images/image-gray-64.flat
rm ./images/image-gray-64.flat.png
rm ./images/image-gray-no-alpha.flat
rm ./images/image-gray-no-alpha.flat.png

./_build/default/bin/rgb2gray.exe --in ./images/image.png --out ./images/image-gray.png --dump-colors
./_build/default/bin/rgb2gray.exe --in ./images/image.png --out ./images/image-gray.flat --outflat --flat-channel y --dump-colors
./_build/default/bin/rgb2gray.exe --in ./images/image-gray.flat --out ./images/image-gray.flat.png --inflat 
./_build/default/bin/rgb2gray.exe --in ./images/image.png --out ./images/image-gray-no-alpha.flat --outflat --no-alpha

./_build/default/bin/rgb2gray.exe --in ./images/image-gray-no-alpha.flat --out ./images/image-gray-no-alpha.flat.png --inflat --no-alpha

./_build/default/bin/rgb2gray.exe --in ./images/image.png --out ./images/image-color.png --color --fgreen 0.2 
./_build/default/bin/rgb2gray.exe --in ./images/image.png --out ./images/image-color-64.png --color --fred 0.8 --fblue 0.5 --fgreen 0.4 --colors 64

./_build/default/bin/rgb2gray.exe --in ./images/image.png --out ./images/image-gray-64.flat --outflat --colors 64
./_build/default/bin/rgb2gray.exe --in ./images/image.png --out ./images/image-gray-8.flat --outflat --colors 8
./_build/default/bin/rgb2gray.exe --in ./images/image-gray-64.flat --out ./images/image-gray-64.flat.png --inflat
./_build/default/bin/rgb2gray.exe --in ./images/image-gray-8.flat --out ./images/image-gray-8.flat.png --inflat
./_build/default/bin/rgb2gray.exe --in ./images/image-gray.flat --out ./images/image-gray-16.flat.png --inflat --colors 16
