open Yojson.Basic.Util

let def_float = -1000.0
let def_int = -1000
let def_string = ""

let field paths json =
  let rec get paths obj =
    match paths with
    | _ when obj = `Null -> obj
    | [] -> obj
    | h :: t -> member h obj |> get t
  in
  get paths json
;;

let field_or_def paths def to_fn json =
  let fld = field paths json in
  match fld with
  | `Null -> def
  | _ -> to_fn fld
;;

module OWM_coord = struct
  type t =
    { lon : float
    ; lat : float
    }

  let coord j =
    let lon = j |> field_or_def [ "coord"; "lon" ] def_float to_number in
    let lat = j |> field_or_def [ "coord"; "lat" ] def_float to_number in
    { lon; lat }
  ;;
end

module OWM_weather = struct
  type t =
    { id : int
    ; main : string
    ; description : string
    ; icon : string
    }

  let weather j =
    let lst = j |> field_or_def [ "weather" ] [] to_list in
    List.(
      fold_left
        (fun acc w ->
          let id = w |> field_or_def [ "id" ] def_int to_int in
          let main = w |> field_or_def [ "main" ] def_string to_string in
          let description = w |> field_or_def [ "description" ] def_string to_string in
          let icon = w |> field_or_def [ "icon" ] def_string to_string in
          { id; main; description; icon } :: acc)
        []
        lst
      |> rev)
  ;;
end

module OWM_main = struct
  type t =
    { temp : float
    ; feels_like : float
    ; temp_min : float
    ; temp_max : float
    ; pressure : int
    ; humidity : int
    ; sea_level : float
    ; grnd_level : float
    }

  let main j =
    let temp = j |> field_or_def [ "main"; "temp" ] def_float to_number in
    let feels_like = j |> field_or_def [ "main"; "feels_like" ] def_float to_number in
    let temp_min = j |> field_or_def [ "main"; "temp_min" ] def_float to_number in
    let temp_max = j |> field_or_def [ "main"; "temp_max" ] def_float to_number in
    let pressure = j |> field_or_def [ "main"; "pressure" ] def_int to_int in
    let humidity = j |> field_or_def [ "main"; "humidity" ] def_int to_int in
    let sea_level = j |> field_or_def [ "main"; "sea_level" ] def_float to_number in
    let grnd_level = j |> field_or_def [ "main"; "grnd_level" ] def_float to_number in
    { temp; feels_like; temp_min; temp_max; pressure; humidity; sea_level; grnd_level }
  ;;
end

module OWM_wind = struct
  type t =
    { speed : float
    ; deg : int
    }

  let wind j =
    let speed = j |> field_or_def [ "wind"; "speed" ] def_float to_number in
    let deg = j |> field_or_def [ "wind"; "deg" ] def_int to_int in
    { speed; deg }
  ;;
end

module OWM_rain = struct
  type t = { _3h : float }

  let rain j =
    let _3h = j |> field_or_def [ "rain"; "3h" ] def_float to_number in
    { _3h }
  ;;

  (*let _3h = field [ "rain"; "3h" ] j |> to_number in*)
end

module OWM_snow = struct
  type t = { _3h : float }

  let snow j =
    let _3h = j |> field_or_def [ "snow"; "3h" ] def_float to_number in
    { _3h }
  ;;
end

module OWM_clouds = struct
  type t = { all : int }

  let clouds j =
    let all = j |> field_or_def [ "wind"; "all" ] def_int to_int in
    { all }
  ;;
end

module OWM_sys = struct
  type t =
    { _type : int
    ; id : int
    ; message : float
    ; country : string
    ; sunrise : int
    ; sunset : int
    }

  let sys j =
    let _type = j |> field_or_def [ "sys"; "type" ] def_int to_int in
    let id = j |> field_or_def [ "sys"; "id" ] def_int to_int in
    let message = j |> field_or_def [ "sys"; "message" ] def_float to_number in
    let country = j |> field_or_def [ "sys"; "country" ] def_string to_string in
    let sunrise = j |> field_or_def [ "sys"; "sunrise" ] def_int to_int in
    let sunset = j |> field_or_def [ "sys"; "sunset" ] def_int to_int in
    { _type; id; message; country; sunrise; sunset }
  ;;
end

module OWM_response = struct
  type t =
    { coord : OWM_coord.t
    ; weather : OWM_weather.t list
    ; base : string
    ; main : OWM_main.t
    ; visibility : int
    ; wind : OWM_wind.t
    ; rain : OWM_rain.t
    ; snow : OWM_snow.t
    ; clouds : OWM_clouds.t
    ; dt : int
    ; sys : OWM_sys.t
    ; timezone : int
    ; id : int
    ; name : string
    ; cod : int
    }

  let response j =
    let coord = OWM_coord.coord j in
    let weather = OWM_weather.weather j in
    let base = j |> field_or_def [ "base" ] def_string to_string in
    let main = OWM_main.main j in
    let visibility = j |> field_or_def [ "visibility" ] def_int to_int in
    let wind = OWM_wind.wind j in
    let rain = OWM_rain.rain j in
    let snow = OWM_snow.snow j in
    let clouds = OWM_clouds.clouds j in
    let dt = j |> field_or_def [ "dt" ] def_int to_int in
    let sys = OWM_sys.sys j in
    let timezone = j |> field_or_def [ "timezone" ] def_int to_int in
    let id = j |> field_or_def [ "id" ] def_int to_int in
    let name = j |> field_or_def [ "name" ] def_string to_string in
    let cod = j |> field_or_def [ "cod" ] def_int to_int in
    { coord
    ; weather
    ; base
    ; main
    ; visibility
    ; wind
    ; rain
    ; snow
    ; clouds
    ; dt
    ; sys
    ; timezone
    ; id
    ; name
    ; cod
    }
  ;;
end
