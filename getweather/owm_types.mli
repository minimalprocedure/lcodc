module OWM_coord : sig
  type t =
    { lon : float
    ; lat : float
    }
end

module OWM_weather : sig
  type t =
    { id : int
    ; main : string
    ; description : string
    ; icon : string
    }
end

module OWM_main : sig
  type t =
    { temp : float
    ; feels_like : float
    ; temp_min : float
    ; temp_max : float
    ; pressure : int
    ; humidity : int
    ; sea_level : float
    ; grnd_level : float
    }
end

module OWM_wind : sig
  type t =
    { speed : float
    ; deg : int
    }
end

module OWM_rain : sig
  type t = { _3h : float }
end

module OWM_snow : sig
  type t = { _3h : float }
end

module OWM_clouds : sig
  type t = { all : int }
end

module OWM_sys : sig
  type t =
    { _type : int
    ; id : int
    ; message: float
    ; country : string
    ; sunrise : int
    ; sunset : int
    }
end

module OWM_response : sig
  type t =
    { coord : OWM_coord.t
    ; weather : OWM_weather.t list
    ; base : string
    ; main : OWM_main.t
    ; visibility : int
    ; wind : OWM_wind.t
    ; rain : OWM_rain.t
    ; snow : OWM_snow.t
    ; clouds : OWM_clouds.t
    ; dt : int
    ; sys : OWM_sys.t
    ; timezone : int
    ; id : int
    ; name : string
    ; cod : int
    }

  val response : Yojson.Basic.t -> t
end
