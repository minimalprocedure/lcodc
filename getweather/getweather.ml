open Lwt

(*open Cohttp*)
open Cohttp_lwt_unix
open Owm_types

let compose_uri key ?country city =
  let api_base = Uri.of_string "http://api.openweathermap.org/data/2.5/weather" in
  let city' =
    match country with
    | Some country -> [ city; country ]
    | None -> [ city ]
  in
  Uri.add_query_params
    api_base
    [ "q", city'; "units", [ "metric" ]; "lang", [ "it" ]; "APPID", [ key ] ]
;;

let request key ?country city =
  let response_handle (response, body) =
    match Response.status response with
    | `OK -> Cohttp_lwt.Body.to_string body
    | _ -> Lwt.fail_with "Not Found"
  in
  let uri = compose_uri key ?country city in
  Client.get uri >>= response_handle
;;

let parse json =
  let json = Yojson.Basic.from_string json in
  OWM_response.response json
;;

let info (r : OWM_response.t) =
  let descriptions =
    List.fold_left (fun d w -> OWM_weather.(w.description) :: d) [] r.weather
  in
  let open Printf in
  let infos =
    [ sprintf "Città: %s - %s" r.name (String.concat "," descriptions)
    ; sprintf "Temperatura: %.2f°" r.main.temp
    ; sprintf "Percepita: %.2f°" r.main.feels_like
    ; sprintf "Minima: %.2f°" r.main.temp_min
    ; sprintf "Massima: %.2f°" r.main.temp_max
    ; sprintf "Pressione: %dhPa" r.main.pressure
    ; sprintf "Umidità: %d%%" r.main.humidity
    ]
  in
  List.iter print_endline infos
;;

let api_key = ref "" (*"8c957dd5fefc4e3624c2248f2aa737d9"*)

let city = ref "Roma"
let country = ref "IT"

let args_parse =
  let opts =
    [ "-c", Arg.Set_string city, ": City"
    ; "-s", Arg.Set_string country, ": State code"
    ; "-k", Arg.Set_string api_key, ": OpenWeatherMap Api Key"
    ]
  in
  let usage =
    "getweather\nusage: "
    ^ Sys.argv.(0)
    ^ " [-c City]"
    ^ " [-s State code]"
    ^ " [-k OpenWeatherMap Api Key]"
  in
  let anons _ = () in
  Arg.parse opts anons usage
;;

let () =
  let json = Lwt_main.run (request !api_key ~country:!country !city) in
  info (parse json)
;;
