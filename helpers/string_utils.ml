let split_string_words_in n s =
  let words = String.split_on_char ' ' s in
  let rev = List.rev in
  let cons_rev r a = rev a :: r in
  let rec spl (i, result, acc) words =
    match words with
    | [] -> cons_rev result acc
    | h :: t ->
      let tp =
        if i >= n then 1, cons_rev result acc, [ h ] else i + 1, result, h :: acc
      in
      spl tp t
  in
  spl (0, [], []) words |> rev
;;

let words s = List.filter (fun c -> c <> "") (String.split_on_char ' ' s)
