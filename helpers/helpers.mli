val read_text_ch: in_channel -> string option
val read_file: string -> string
val block_stream_of_ch: in_channel -> string Seq.t
val process_file_content: op:(string -> unit) -> string -> unit
